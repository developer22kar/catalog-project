# README #


### What is this repository for? ###

* Book catalog with admin and regular user interfaces.
* 1.0


### How do I get set up? ###

* Git clone the project
* Go to catalog-root folder
* Run mvn clean install
* Under catalog-databases, go to dbscripts and run the script to get the db     up and running.
* Copy wars in a tomcat7+ container. Have fun

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines