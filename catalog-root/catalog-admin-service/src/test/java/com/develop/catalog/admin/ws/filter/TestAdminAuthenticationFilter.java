package com.develop.catalog.admin.ws.filter;

import com.develop.catalog.utilities.dao.UserAuthDAO;
import com.develop.catalog.utilities.dao.UserDAO;
import com.develop.catalog.utilities.json.ResponseJson;
import com.develop.catalog.utilities.model.User;
import com.develop.catalog.utilities.model.UserAuthentication;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class TestAdminAuthenticationFilter {

	private String VALID_TOKEN = "valid_token";
	private String INVALID_TOKEN = "invalid_token";
	private String MOCK_PATH = "mock_path";
	
	@Mock
	private ContainerRequestContext requestContext;
	@Mock
	private UriInfo uriInfo;
	@Mock
	private ResponseJson responseJson;
	@Mock
	private UserDAO userDAO;
	@Mock
	private UserAuthDAO userAuthenticationDAO;
	
	private ArgumentCaptor<Response> argumentCaptor;
	private AdminAuthenticationFilter adminAuthFilter;
	
	@Before
	public void setup() {
		
		argumentCaptor = ArgumentCaptor.forClass(Response.class);
		adminAuthFilter = new AdminAuthenticationFilter();
		MockitoAnnotations.initMocks(this);
		Mockito.when(uriInfo.getPath()).thenReturn(MOCK_PATH);
		Mockito.when(requestContext.getUriInfo()).thenReturn(uriInfo);

		when(responseJson.addStatus(anyBoolean())).thenReturn(responseJson);
		when(responseJson.addResponseMessage(anyString())).thenReturn(responseJson);
		when(responseJson.build(anyObject())).thenReturn("response");
		adminAuthFilter.setResponseJson(responseJson);
		adminAuthFilter.setUserDAO(userDAO);
		adminAuthFilter.setUserAuthenticationDAO(userAuthenticationDAO);
	}
	
	/**
	 * This method is a test for no tokens found in the URL.
	 * @throws Exception
	 */
	@Test
	public void testFilter_NoTokenInURL() throws Exception {
		adminAuthFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),401);
		String json = (String) argumentCaptor.getValue().getEntity(); 
		assertTrue(json instanceof String);
	}
	
	/**
	 * This method is a test for no tokens found in the login URL.
	 * @throws Exception
	 */
	@Test
	public void testFilter_NoTokenInLoginCallURL() throws Exception {
		Mockito.when(uriInfo.getPath()).thenReturn("../login");
		adminAuthFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
		
	}
	
	/**
	 * 	This method is a test for tokens found in the login URL.
	 * @throws Exception
	 */
	@Test
	public void testFilter_TokenLoginCallURL() throws Exception {
		when(requestContext.getHeaderString("auth_token")).thenReturn(INVALID_TOKEN);
		Mockito.when(uriInfo.getPath()).thenReturn("../login");
		adminAuthFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
	}
	
	/**
	 * This method is a test for invalid token and bad request.
	 * @throws Exception
	 */
	@Test
	public void testFilter_TokenInvalidAndBadRequest() throws Exception {
		InputStream is = IOUtils.toInputStream("");
		when(requestContext.getHeaderString("auth_token")).thenReturn(INVALID_TOKEN);
		when(requestContext.getEntityStream()).thenReturn(is);
		adminAuthFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),401);
	}
	/**
	 * This method is a test for invalid token and user not found in database.
	 * @throws Exception
	 */
	@Test
	public void testFilter_TokenInvalidAndUserFromBodyNotFoundInDB() throws Exception {
		
		InputStream is = IOUtils.toInputStream("{\r\n  \"emailID\": \"mkelkar@test.com\",\r\n  \"password\": \"12345#\"\r\n}");
		when(requestContext.getHeaderString("auth_token")).thenReturn("faketoken");
		when(requestContext.getEntityStream()).thenReturn(is);
		when(userDAO.findByEmailID((String)anyObject())).thenReturn(null);
		
		adminAuthFilter.filter(requestContext);
		
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),401);
	}
	
	/**
	 * This method is the test for token invalid and user found in the database.
	 * @throws Exception
	 */
	@Test
	public void testFilter_TokenInvalidAndUserFromBodyFoundInDB() throws Exception {
		
		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("mkelkar@test.com");
		user1.setFirstName("testName");
		user1.setPassword("testPassword");

		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken("expected_token");

		
		InputStream is = IOUtils.toInputStream("{\r\n  \"emailID\": \"mkelkar@test.com\",\r\n  \"password\": \"testPassword\"\r\n}");
		when(requestContext.getHeaderString("auth_token")).thenReturn("faketoken");
		when(requestContext.getEntityStream()).thenReturn(is);
		when(userDAO.findByEmailID("mkelkar@test.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);
		adminAuthFilter.filter(requestContext);
		
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),401);
	}
	
	/**
	 * This method is the test for fetching user details by user email id.
	 * @throws IOException
	 */
	@Test
	public void testFilter_GetUserByEmailID() throws IOException {

		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("mkelkar@test.com");
		user1.setFirstName("testName");
		user1.setPassword("testPassword");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken(VALID_TOKEN);
		uAuthData.setSessionStartTime(new Date());

		when(requestContext.getHeaderString("auth_token")).thenReturn(VALID_TOKEN);
		Mockito.when(uriInfo.getPath()).thenReturn("../getUser/mkelkar@test.com");
		when(userDAO.findByEmailID("mkelkar@test.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);
        when(userAuthenticationDAO.findBySessionToken(anyString())).thenReturn(uAuthData);
		adminAuthFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
	}
	
	/**
	 * This method is the test for getting user by email Id when provided id is null.
	 * @throws IOException
	 */
	@Test
	public void testFilter_GetUserByEmailIDEmailIDNull() throws IOException {

		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("mkelkar@test.com");
		user1.setFirstName("testName");
		user1.setPassword("testPassword");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken(VALID_TOKEN);
		uAuthData.setSessionStartTime(new Date());

		when(requestContext.getHeaderString("auth_token")).thenReturn(VALID_TOKEN);
		Mockito.when(uriInfo.getPath()).thenReturn("../getUser/");
		when(userDAO.findByEmailID("mkelkar@test.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);

		adminAuthFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),401);
	}

	/**
	 * This method is the test for getting user by email Id when provided id is not found.
	 * @throws IOException
	 */
	@Test
	public void testFilter_GetUserByEmailIDEmailNotFound() throws IOException {

		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("mkelkar@test.com");
		user1.setFirstName("testName");
		user1.setPassword("testPassword");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken(VALID_TOKEN);
		uAuthData.setSessionStartTime(new Date());

		when(requestContext.getHeaderString("auth_token")).thenReturn(VALID_TOKEN);
		Mockito.when(uriInfo.getPath()).thenReturn("../getUser/test@test.com");
		when(userDAO.findByEmailID("mkelkar@test.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);

		adminAuthFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(401,argumentCaptor.getValue().getStatus());
	}

	/**
	 * This method is the test for getting user by email Id when provided id is invalid.
	 * @throws IOException
	 */
	@Test
	public void testFilter_GetUserByEmailIDInValidEmailID() throws IOException {

		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("mkelkar@test.com");
		user1.setFirstName("testName");
		user1.setPassword("testPassword");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken(VALID_TOKEN);
		uAuthData.setSessionStartTime(new Date());

		when(requestContext.getHeaderString("auth_token")).thenReturn(VALID_TOKEN);
		Mockito.when(uriInfo.getPath()).thenReturn("../getUser/test#testing");
		when(userDAO.findByEmailID("mkelkar@test.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);

		adminAuthFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(401,argumentCaptor.getValue().getStatus());
	}

	/**
	 * This method is the test for getting user by email Id when session is invalid.
	 * @throws IOException
	 */
	@Test
	public void testFilter_TokenValidAndUserFromBodyFoundInDBButSessionInValid() throws Exception {
		
		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("mkelkar@test.com");
		user1.setFirstName("testName");
		user1.setPassword("testPassword");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken(VALID_TOKEN);
		uAuthData.setSessionStartTime(cal.getTime());

		
		InputStream is = IOUtils.toInputStream("{\r\n  \"emailID\": \"mkelkar@test.com\",\r\n  \"password\": \"testPassword\"\r\n}");
		when(requestContext.getHeaderString("auth_token")).thenReturn(VALID_TOKEN);
		when(requestContext.getEntityStream()).thenReturn(is);
		when(userDAO.findByEmailID("mkelkar@test.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);
		adminAuthFilter.filter(requestContext);
		
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),401);

	}

	/**
	 * This method is the test when all the provided data is valid.
	 * @throws IOException
	 */
	@Test
	public void testFilter_GoodData() throws Exception {
		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("mkelkar@test.com");
		user1.setFirstName("Amar");
		user1.setPassword("testPassword");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken(VALID_TOKEN);
		uAuthData.setSessionStartTime(new Date());

		
		InputStream is = IOUtils.toInputStream("{\r\n  \"emailID\": \"mkelkar@test.com\",\r\n  \"password\": \"testPassword\"\r\n}");
		when(requestContext.getHeaderString("auth_token")).thenReturn(VALID_TOKEN);
		when(requestContext.getEntityStream()).thenReturn(is);
		when(userDAO.findByEmailID("mkelkar@test.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);
        when(userAuthenticationDAO.findBySessionToken(anyString())).thenReturn(uAuthData);
		adminAuthFilter.filter(requestContext);
		
		verify(requestContext, never()).abortWith((Response)anyObject());
	}
}
