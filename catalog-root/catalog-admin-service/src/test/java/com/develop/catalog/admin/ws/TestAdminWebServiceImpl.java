package com.develop.catalog.admin.ws;

import com.develop.catalog.utilities.dao.*;
import com.develop.catalog.utilities.json.SearchParams;
import com.develop.catalog.utilities.model.*;
import com.develop.catalog.utilities.security.MessageDigest;
import org.apache.commons.codec.binary.Hex;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:TestAdminApplication-context.xml"})
public class TestAdminWebServiceImpl extends JerseyTest {
	
	User adminUser, user2;

	UserAuthentication userAuth1;

	@Autowired
	private UserAuthDAO userAuthenticationDAO;

	@Autowired
	private UserDAO userDAO;

    @Autowired
    private RoleDAO roleDAO;

    @Autowired
    private UserRoleDAO userRoleDAO;

    @Autowired
    private AuthorDAO authorDAO;
    @Autowired
    private AuthorBookDAO authorBookDAO;
    @Autowired
    private BookDAO bookDAO;


    @Before
	public void setup() {
		
		MockitoAnnotations.initMocks(this);
		adminUser = new  User();
		user2 = new  User();
		userAuth1 = new UserAuthentication();
		
		adminUser.setUserID(1);
		adminUser.setEmailID("paplu@gmail.com");
		adminUser.setFirstName("Amar");
		adminUser.setPassword("aailaUiim@1");
		adminUser.setCreatedBy(1);
		adminUser.setLastModifiedBy(1);
        adminUser.setCreatedOn(new Date());
        adminUser.setLastModifiedOn(new Date());


        user2.setUserID(2);
		user2.setEmailID("taplu@gmail.com");
		user2.setFirstName("Prem");
		user2.setPassword("aailaUiim#2");
		user2.setCreatedBy(1);
		user2.setLastModifiedBy(1);

		userDAO.saveOrUpdate(adminUser);

		userAuth1.setUserAuthID(1);
		userAuth1.setUser(adminUser);
		userAuth1.setSessionAuthToken("lalala");
		userAuth1.setPasswordSaltKey("lala");
		userAuth1.setCreatedBy(1);
		userAuth1.setLastModifiedBy(1);
		userAuthenticationDAO.saveOrUpdate(userAuth1);

        Role role = new Role();
        role.setRoleName("Admin");
        role.setRoleDescription("admin baba in system");
        roleDAO.saveOrUpdate(role);

        //make dude admin
        UserRole userRole = new UserRole();
        userRole.setRole(role);
        userRole.setUser(adminUser);
        userRoleDAO.saveOrUpdate(userRole);


        //create user in system
		target("/admin/users/create").request().header("auth_token", "lalala").post(Entity.json(user2), String.class);
	}
	

    @Test
    public void testGetUserList_ValidSearchParamsNoUserFound() {
        SearchParams params = new SearchParams();
        params.setSearchText("pri");
        params.setStatusCodeID("10002");
        params.setPageNumber(1);
        params.setPageSize(50);
        params.setSortByColumnName("firstName");
        Response response = target("/admin/users/list").request().header("auth_token", "lalala").post(Entity.json(params), Response.class);
        String responseJson = response.readEntity(String.class);
        assertEquals(404,response.getStatus());

    }


    @Test
    public void testLoginUser_UserNotFound() {
        User user = new User();
        user.setEmailID("ramlal@gmail.com"); // ramlal doesnt exists
        user.setPassword("password");
        Response response = target("/admin/users/login").request().put(Entity.json(user), Response.class);
        assertEquals(401, response.getStatus());

    }

    @Test
    public void testLoginUser_UserFoundButNotAnAdmin() {
        User user = new User();
        user.setEmailID("taplu@gmail.com"); // taplu is a regular user
        user.setPassword("password");
        Response response = target("/admin/users/login").request().put(Entity.json(user), Response.class);
        assertEquals(401, response.getStatus());


    }

    @Test
    public void testLoginUser_UserAdminButNoRolesSetYet() {
        // this happens when admin is newly created in the system and he does not have
        // sufficient role/permissions set up
        User user = new User();
        user.setEmailID("paplu@gmail.com"); // paplu is admin without role/permissions
        user.setPassword("password");
        Response response = target("/admin/users/login").request().put(Entity.json(user), Response.class);
        assertEquals(401, response.getStatus());


    }

    @Test
    public void testLoginUser_UserFoundAnsIsAdminButInvalidPassword(){
        // set up a role for paplu
        UserRole role = new UserRole();
        role.setUser(adminUser);
        userRoleDAO.saveOrUpdate(role);
        User user = new User();
        user.setEmailID("paplu@gmail.com"); // paplu is admin without role/permissions
        user.setPassword("password");
        Response response = target("/admin/users/login").request().put(Entity.json(user), Response.class);
        assertEquals(401, response.getStatus());

    }

    @Test
    public void testLoginUser_UserFoundAnsIsAdminAndValidPassword(){

        Role role = new Role();
        role.setRoleName("Admin1");
        role.setRoleDescription("admin baba in system");
        roleDAO.saveOrUpdate(role);

        User user = new User();
        user.setEmailID("pritesh@gmail.com");
        try {
           String password =  Hex.encodeHexString(MessageDigest.getDigest("pritesh123lala", "SHA-256"));
           user.setPassword(password);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        userDAO.saveOrUpdate(user);

        UserAuthentication userAuth = new UserAuthentication();
        userAuth.setUser(user);
        userAuth.setPasswordSaltKey("lala");
        userAuthenticationDAO.saveOrUpdate(userAuth);

        //make dude admin
        UserRole userRole = new UserRole();
        userRole.setRole(role);
        userRole.setUser(user);
        userRoleDAO.saveOrUpdate(userRole);

        // login as newly created admin user in system
        User tryingUser = new User();
        tryingUser.setEmailID("pritesh@gmail.com");
        tryingUser.setPassword("pritesh123");

        Response response = target("/admin/users/login").request().put(Entity.json(tryingUser), Response.class);
        assertEquals(200, response.getStatus());

    }

    @Test
    public void testCreateUser_UserValidationFailed() {
        User user = new User();
        user.setEmailID("pdsfdsg");
        user.setPassword("dfgdfg");
        Response response = target("/admin/users/create").request().header("auth_token", "lalala").post(Entity.json(user), Response.class);
        assertEquals(response.getStatus(),Response.Status.BAD_REQUEST.getStatusCode());

    }

    @Test
    public void testCreateUser_InvalidAdminSessionToken() {
        User user = new User();
        user.setEmailID("prit@gmail.com");
        user.setPassword("password123");
        Response response = target("/admin/users/create").request().header("auth_token", "blalala").post(Entity.json(user), Response.class);
        assertEquals(response.getStatus(),Response.Status.BAD_REQUEST.getStatusCode());


    }

    @Test
    public void testCreateUser_Success() {
        User user = new User();
        user.setEmailID("prit@gmail.com");
        user.setPassword("password123");
        Response response = target("/admin/users/create").request().header("auth_token", "lalala").post(Entity.json(user), Response.class);
        assertEquals(response.getStatus(), Response.Status.CREATED.getStatusCode());
        assertTrue(response.readEntity(String.class).contains("created"));

        User tryingUserFromDB = userDAO.findByEmailID(user.getEmailID());
        assertEquals(adminUser.getUserID(),tryingUserFromDB.getCreatedBy());

    }

    @Test
    public void testCreateUser_UserExists() {
        User user = new User();
        user.setEmailID("prit@gmail.com");
        user.setPassword("password123");
        Response response = target("/admin/users/create").request().header("auth_token", "lalala").post(Entity.json(user), Response.class);
        assertEquals(response.getStatus(), Response.Status.CREATED.getStatusCode());
        assertTrue(response.readEntity(String.class).contains("created"));

        User tryingUserFromDB = userDAO.findByEmailID(user.getEmailID());
        assertEquals(adminUser.getUserID(),tryingUserFromDB.getCreatedBy());


        Response newResponse = target("/admin/users/create").request().header("auth_token", "lalala").post(Entity.json(user), Response.class);
        assertEquals(newResponse.getStatus(), Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    public void testUpdateUser_UserNotFound() {
        User user = new User();
        user.setEmailID("pritesh@gmail.com");
        Response newResponse = target("/admin/users/update").request().header("auth_token", "lalala").put(Entity.json(user), Response.class);
        assertEquals(newResponse.getStatus(),404);
    }


    @Test
    public void testDeleteUser_UserFoundValidSessionToken() {
        User user = new User();
        user.setEmailID("pritesh@gmail.com");
        user.setPassword("pritesh123");
        target("/admin/users/create").request().header("auth_token", "lalala").post(Entity.json(user), Response.class);

        User userFromDB = userDAO.findByEmailID(user.getEmailID());
        String id =  new Integer(userFromDB.getUserID()).toString();
        Response newResponse = target("/admin/users/delete/"+id).request().header("auth_token", "lalala").delete();
        User deletedUser = userDAO.findByEmailID(user.getEmailID());
        assertEquals(newResponse.getStatus(),200);
        assertEquals(adminUser.getUserID(),deletedUser.getLastModifiedBy());


    }

    @Test
    public void testDeleteUser_UserFoundInValidSessionToken() {
        User user = new User();
        user.setEmailID("pritesh@gmail.com");
        user.setPassword("pritesh123");
        target("/admin/users/create").request().header("auth_token", "lalala").post(Entity.json(user), Response.class);

        User userFromDB = userDAO.findByEmailID(user.getEmailID());
        String id =  new Integer(userFromDB.getUserID()).toString();
        Response newResponse = target("/admin/users/delete/"+id).request().header("auth_token", "lahhlala").delete();
        assertEquals(newResponse.getStatus(),400);
        User deletedUser = userDAO.findByEmailID(user.getEmailID());


    }

    @Test
    public void testCreateBook() {

        Author author = new Author();
        author.setFirstName("Brian");
        author.setLastName("Goetz");
        author.setEmailID("b@gmail.com");
        authorDAO.saveOrUpdate(author);
        Book book = new Book();
        book.setTitle("Java concurrency");
        book.setISBN("9780201657883");
        book.setPublisher("Addison");
        book.setPrice(new BigDecimal(10));
        book.setAuthor("Brian Goetz");
        Response response = target("/admin/book/create").request().header("auth_token", "lalala").post(Entity.json(book), Response.class);
        String output = response.readEntity(String.class);
        assertEquals(200,response.getStatus());
        assertTrue(output.contains("added"));


    }

    @Test
    public void testCreateBook_UserNotAdmin() {

        Author author = new Author();
        author.setFirstName("Brian");
        author.setLastName("Goetz");
        author.setEmailID("b@gmail.com");
        authorDAO.saveOrUpdate(author);
        Book book = new Book();
        book.setTitle("Java concurrency");
        book.setISBN("9780201657883");
        book.setPublisher("Addison");
        book.setPrice(new BigDecimal(10));
        book.setAuthor("Brian Goetz");
        Response response = target("/admin/book/create").request().header("auth_token", "lalafdgfla").post(Entity.json(book), Response.class);
        String output = response.readEntity(String.class);
        assertEquals(400,response.getStatus());
        assertTrue(output.contains("invalid"));


    }

    @Test
    public void testCreateBook_ISBNExists() {

        Author author = new Author();
        author.setFirstName("Brian");
        author.setLastName("Goetz");
        author.setEmailID("b@gmail.com");
        authorDAO.saveOrUpdate(author);
        Book book = new Book();
        book.setTitle("Java concurrency");
        book.setISBN("9780201657883");
        book.setPublisher("Addison");
        book.setPrice(new BigDecimal(10));
        book.setAuthor("Brian Goetz");
        bookDAO.saveOrUpdate(book);

        AuthorBook ab = new AuthorBook();
        ab.setAuthor(author);
        ab.setBook(book);

        authorBookDAO.saveOrUpdate(ab);

        Response response = target("/admin/book/create").request().header("auth_token", "lalala").post(Entity.json(book), Response.class);
        String output = response.readEntity(String.class);
        assertEquals(409,response.getStatus());
        assertTrue(output.contains("ISBN"));

    }

    @Test
    public void testCreateBook_InvalidBookParams() {

        Book book = new Book();
        Response response = target("/admin/book/create").request().header("auth_token", "lalala").post(Entity.json(book), Response.class);
        String output = response.readEntity(String.class);
        assertEquals(400,response.getStatus());
        assertTrue(output.contains("params"));
    }

    @Test
    public void testBookUpdate_Success() {

        Author author = new Author();
        author.setFirstName("Brian");
        author.setLastName("Goetz");
        author.setEmailID("b@gmail.com");
        authorDAO.saveOrUpdate(author);
        Book book = new Book();
        book.setTitle("Java concurrency");
        book.setISBN("9780201657883");
        book.setPublisher("Addison");
        book.setPrice(new BigDecimal(10));
        book.setAuthor("Brian Goetz");
        bookDAO.saveOrUpdate(book);
        AuthorBook ab = new AuthorBook();
        ab.setAuthor(author);
        ab.setBook(book);
        authorBookDAO.saveOrUpdate(ab);

        Author p = new Author();
        p.setFirstName("Pritesh");
        p.setLastName("B");
        p.setEmailID("pritesh@gmail.com");
        authorDAO.saveOrUpdate(p);

        Book b = new Book();
        b.setAuthor("Pritesh B");
        b.setBookID(bookDAO.findByISBN("9780201657883").getBookID());

        Response response = target("/admin/book/update").request().header("auth_token", "lalala").post(Entity.json(b), Response.class);
        assertEquals(200,response.getStatus());
    }


    @Test
    public void testUpdate_Fail() {
        Author author = new Author();
        author.setFirstName("Brian");
        author.setLastName("Goetz");
        author.setEmailID("b@gmail.com");
        authorDAO.saveOrUpdate(author);
        Book book = new Book();
        book.setTitle("Java concurrency");
        book.setISBN("9780201657883");
        book.setPublisher("Addison");
        book.setPrice(new BigDecimal(10));
        book.setAuthor("Brian Goetz");
        bookDAO.saveOrUpdate(book);
        AuthorBook ab = new AuthorBook();
        ab.setAuthor(author);
        ab.setBook(book);
        authorBookDAO.saveOrUpdate(ab);

        Author p = new Author();
        p.setFirstName("Pritesh");
        p.setLastName("B");
        p.setEmailID("pritesh@gmail.com");
        authorDAO.saveOrUpdate(p);

        Book b = new Book();
        b.setAuthor("Pritesh B");
        b.setISBN("2738462743");
        b.setBookID(bookDAO.findByISBN("9780201657883").getBookID());
        Response response = target("/admin/book/update").request().header("auth_token", "lalala").post(Entity.json(b), Response.class);
        assertEquals(304,response.getStatus());
    }

    @Test
    public void testDeleteBook_Success() {
        Author author = new Author();
        author.setFirstName("Brian");
        author.setLastName("Goetz");
        author.setEmailID("b@gmail.com");
        authorDAO.saveOrUpdate(author);
        Book book = new Book();
        book.setTitle("Java concurrency");
        book.setISBN("9780201657883");
        book.setPublisher("Addison");
        book.setPrice(new BigDecimal(10));
        book.setAuthor("Brian Goetz");
        bookDAO.saveOrUpdate(book);
        AuthorBook ab = new AuthorBook();
        ab.setAuthor(author);
        ab.setBook(book);
        authorBookDAO.saveOrUpdate(ab);

        assertNotNull(bookDAO.findById(1));
        Response response = target("/admin/book/delete/1").request().header("auth_token", "lalala").delete();
        assertEquals(200,response.getStatus());
        assertNull(bookDAO.findById(1));
    }

    @Test
    public void testDeleteBook_Fail() {
        Response response = target("/admin/book/delete/123").request().header("auth_token", "lalala").delete();
        assertEquals(404,response.getStatus());
        assertNull(bookDAO.findById(1));
        assertNull(bookDAO.findById(123));
    }

    @Test
    public void addDuplicateAuthor() {
        Author author = new Author();
        author.setFirstName("Brian");
        author.setLastName("Goetz");
        author.setEmailID("b@gmail.com");
        authorDAO.saveOrUpdate(author);

        Author a = new Author();
        a.setFirstName("Brian");
        a.setLastName("Goetz");
        a.setEmailID("b@gmail.com");


        Response response = target("/admin/author/add").request().header("auth_token", "lalala").post(Entity.json(a), Response.class);
        assertEquals(400,response.getStatus());
    }

    @Override
    protected Application configure() {
        ResourceConfig rc = new ResourceConfig(AdminWebServiceImpl.class);
        rc.property("contextConfigLocation", "classpath*:TestAdminApplication-context.xml");
        return rc;

    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public void setAuthorBookDAO(AuthorBookDAO authorBookDAO) {
        this.authorBookDAO = authorBookDAO;
    }

    public void setBookDAO(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }

    public void setUserAuthenticationDAO(UserAuthDAO userAuthenticationDAO) {
		this.userAuthenticationDAO = userAuthenticationDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

    public void setUserRoleDAO(UserRoleDAO userRoleDAO) {
        this.userRoleDAO = userRoleDAO;
    }

    public void setRoleDAO(RoleDAO roleDAO) {
        this.roleDAO = roleDAO;
    }


}
