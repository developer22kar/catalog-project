package com.develop.catalog.admin.ws.filter;

import com.develop.catalog.admin.ws.filter.constants.AdminAuthenticationFilterConstants;
import com.develop.catalog.utilities.dao.UserAuthDAO;
import com.develop.catalog.utilities.dao.UserDAO;
import com.develop.catalog.utilities.json.ResponseJson;
import com.develop.catalog.utilities.model.User;
import com.develop.catalog.utilities.model.UserAuthentication;
import com.develop.catalog.ws.filter.constants.AuthenticationFilterConstants;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Provider
@PreMatching
public class AdminAuthenticationFilter implements ContainerRequestFilter
{
	@Autowired
	UserAuthDAO userAuthenticationDAO;

	@Autowired
	UserDAO userDAO;

	@Autowired
	ResponseJson responseJson;
	
	private Log log = LogFactory.getLog(AdminAuthenticationFilter.class);

    @Override
	public void filter(ContainerRequestContext containerRequestContext) throws IOException 
	{
		String path = containerRequestContext.getUriInfo().getPath();
		log.info("The path in the URL : "+path);
		String response = "";
		if(!path.contains(AdminAuthenticationFilterConstants.WEBCALL_LOGIN))
		{
			String authenticationToken = containerRequestContext.getHeaderString(AdminAuthenticationFilterConstants.AUTHENTICATION_TOKEN);
			if(authenticationToken != null && !authenticationToken.isEmpty())
			{
				log.info("Authentication token is present in the header of the request.");

                // admin filter wont necessarily have user data in requests. Rely on auth_token only.
                // search for auth_token in userAuth table for validation.
                UserAuthentication userAuthData = userAuthenticationDAO.findBySessionToken(authenticationToken);
                if(userAuthData != null) {
                    // so a valid token is present in db. This token was generated when user login was successful.
                    // this means that the user is authenticated and authorized if he made it this far.
                    // check now if the token has not expired.
                    if(!isSessionValid(userAuthData)) {
						log.info("Session token has expired.");
						response = responseJson.addStatus(false).addResponseMessage("Token is invalid or expired").build(null);
						containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(AdminAuthenticationFilterConstants.MEDIA_TYPE_JSON).build());
                    }

                } else {
                    // token was invalid or expired. get out.
                    log.info("Token is invalid.");
					response = responseJson.addStatus(false).addResponseMessage("Token is invalid or expired").build(null);
					containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(AdminAuthenticationFilterConstants.MEDIA_TYPE_JSON).build());

                }

			}
			else 
			{
				log.info("Authentication token not found with request. Response code : 401");
				response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.TOKEN_NOT_FOUND).build(null);
				containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(AdminAuthenticationFilterConstants.MEDIA_TYPE_JSON).build());
			}
		}
	}
	private User getRequestBody(ContainerRequestContext containerRequestContext) {

		String json = "";
		try 
		{
			if(containerRequestContext.getEntityStream() != null) 
			{
				json = IOUtils.toString(containerRequestContext.getEntityStream());
				InputStream inputStream =  IOUtils.toInputStream(json);
				containerRequestContext.setEntityStream(inputStream);
			} 
			else 
			{
				return null;
			}
		} 
		catch (IOException e) 
		{
			log.info("Error getting json:: " + e.getMessage());
		}
		User user = new Gson().fromJson(json, User.class);
		return user;
	}
	private UserAuthentication getUserAuthData(String userEmailID) 
	{
		User fetchedUser = userDAO.findByEmailID(userEmailID);
		if(fetchedUser != null) 
		{
			UserAuthentication userAuthData = userAuthenticationDAO.findByUserID(fetchedUser.getUserID());
			return userAuthData;
		}
		return null;
	}
	private void authenticateAndAuthorizeRequest(ContainerRequestContext requestContext, String emailAddress, String authToken)
	{
		String response = "";
		UserAuthentication userAuthData = getUserAuthData(emailAddress);

		if(userAuthData != null) 
		{
			String expectedToken = userAuthData.getSessionAuthToken();
			log.info("Expected session token in DB:: " +expectedToken);
			log.info("Authentication tokwn in the request header:: " +authToken);
			
			if(expectedToken != null)
			{
				if(!expectedToken.equals(authToken))
				{
					log.info("Token is invalid.");
					response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.INVALID_TOKEN).build(null);
					requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(AdminAuthenticationFilterConstants.MEDIA_TYPE_JSON).build());

				} 
				else
				{
					// token is valid. Check if its expired. 
					if(!isSessionValid(userAuthData))
					{
						log.info("Session token has expired. Please login again.");
						response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.INVALID_TOKEN).build(null);
						requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(AdminAuthenticationFilterConstants.MEDIA_TYPE_JSON).build());
					}
				}
			}
			else 
			{
				// user has never logged in. Probably a new user in system.
				log.info("Token is invalid. Go Home!");
				response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.INVALID_TOKEN).build(null);
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(AdminAuthenticationFilterConstants.MEDIA_TYPE_JSON).build());
			}

		} 
		else 
		{
			// user data or userauth data not found. return 404
			log.info("User not authorized or user not found.");
			response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.USER_UNAUTHORIZED).build(null);
			requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).entity(response).type(AdminAuthenticationFilterConstants.MEDIA_TYPE_JSON).build());
		}
	}

	private String parsePathForEmailID(String path) 
	{
		return path.substring(path.lastIndexOf("/")+1);
	}

	private UserAuthentication getUserAuthData(User user)
	{
		String userEmailID = user.getEmailID();
		User fetchedUser = userDAO.findByEmailID(userEmailID);
		if(fetchedUser != null) 
		{
			UserAuthentication userAuthData = userAuthenticationDAO.findByUserID(fetchedUser.getUserID());//.getSessionAuthToken();
			return userAuthData;
		}
		return null;
	}
	
	private boolean isSessionValid(UserAuthentication userAuthData) 
	{
		Date sessionStartTime = userAuthData.getSessionStartTime();
		Date currentTime = new Date();
		long timeDifference = currentTime.getTime() - sessionStartTime.getTime();

		if(TimeUnit.MILLISECONDS.toMinutes(timeDifference) > AdminAuthenticationFilterConstants.SESSION_EXPIRY_TIME) 
		{
			return false;
		} 
		else 
		{
			return true;
		}
	}
	public void setUserAuthenticationDAO(UserAuthDAO userAuthenticationDAO) 
	{
		this.userAuthenticationDAO = userAuthenticationDAO;
	}

	public void setUserDAO(UserDAO userDAO) 
	{
		this.userDAO = userDAO;
	}

	public void setResponseJson(ResponseJson responseJson)
	{
		this.responseJson = responseJson;
	}
}
