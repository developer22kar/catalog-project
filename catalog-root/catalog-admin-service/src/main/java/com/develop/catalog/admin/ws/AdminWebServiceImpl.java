package com.develop.catalog.admin.ws;

import com.develop.catalog.admin.service.AdminService;
import com.develop.catalog.service.exception.AuthorDoesNotExistsException;
import com.develop.catalog.service.exception.InputValidationException;
import com.develop.catalog.service.exception.SessionTokenInvalidException;
import com.develop.catalog.service.exception.UserValidationException;
import com.develop.catalog.utilities.json.ResponseBook;
import com.develop.catalog.utilities.json.ResponseJson;
import com.develop.catalog.utilities.model.Author;
import com.develop.catalog.utilities.model.Book;
import com.develop.catalog.utilities.model.User;
import com.develop.catalog.ws.service.BookService;
import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.Map;

@Component
@Path("/admin")
public class AdminWebServiceImpl implements AdminWebService{

	private static Log log = LogFactory.getLog(AdminWebServiceImpl.class);
	private Map<String, Object> customResponseMap = Maps.newHashMap();
	
	@Autowired
	private AdminService adminService;
	@Autowired
	private ResponseJson responseJson;

    @Autowired
    private BookService bookService;

    @PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("users/login")
	public Response loginUser(User user) {

		log.info("Attempting to log in an admin user.");
		String response = "";
		String authenticationToken = adminService.loginAdminUser(user);

		if(authenticationToken != null) {
			log.info("Admin User authenticated- token:: "+ authenticationToken);
			response = responseJson.addStatus(true).addResponseMessage("User login success: 001").build(authenticationToken);
			return Response.status(Response.Status.OK).entity(response).build();
		} else {
			log.info("Admin User credentials invalid or not enough privileges to login. Unauthorized- Go home! ");
			response = responseJson.addStatus(false).addResponseMessage("User login failed: 002").build(null);
			return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
		}
	}
	
	

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("users/create")
	public Response createUser(@Context HttpHeaders httpHeaders, User user) {

		String response = "";
		log.info("Creating new user - " + user);
		
		try {
			if(adminService.createUser(httpHeaders,user)){
				log.info("New user created - " + user);
				response = responseJson.addStatus(true).addResponseMessage("0-01-023").build(user);
				return Response.status(Status.CREATED).entity(response).build();
			} else {
				response = responseJson.addStatus(false).addResponseMessage("0-01-024").build(null);
				return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
			} 
		}catch(UserValidationException ex) {
			log.info("User validation failed- " + ex.getMessage());
			response = responseJson.addStatus(false).addResponseMessage("0-01-025").build(null);
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        }catch (SessionTokenInvalidException ex) {
            log.info("Session token has expired or is invalid- " + ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage("0-01-003").build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        }

    }
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("users/update")
	public Response updateUser(@Context HttpHeaders httpHeaders, User user) {
		
		String response = "";
		try{
			log.info("Updating user: "+ user);
			User updatedUser = adminService.updateUser(httpHeaders, user);
			if(updatedUser != null) {
				customResponseMap.clear();
				if(user.getFirstName() != null) {
					customResponseMap.put("firstName", updatedUser.getFirstName());
				}
				if(user.getLastName() != null) {
					customResponseMap.put("lastName", updatedUser.getLastName());
				}
				if(user.getPassword() != null) {
					customResponseMap.put("password", updatedUser.getPassword());
				}
				response = responseJson.addStatus(true).addResponseMessage("0-01-019").buildCustomObject(User.class, customResponseMap);
				return Response.status(Response.Status.OK).entity(response).build();
			} else {
				log.info("User does not exists! -  "+ user);
				response = responseJson.addStatus(false).addResponseMessage("0-01-020").build(null);
				return Response.status(Response.Status.NOT_FOUND).entity(response).build();
			}
			
		}catch(UserValidationException ex) {
			log.info("User validation failed! -  "+ ex.getMessage());
			response = responseJson.addStatus(false).addResponseMessage("0-01-025").build(null);
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        } catch (SessionTokenInvalidException ex) {
            log.info("Session validation failed -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage("0-01-003").build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        }
    }
		
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("users/delete/{id}")
	public Response deleteUserById(@Context HttpHeaders httpHeaders, @PathParam("id") Integer id) {

		String response = "";
		try {
			log.info("Deleting user by id- " + id);
			if(adminService.deleteUserById(httpHeaders, id)){
				log.info("UserID marked as deleted in the system - " + id);
                response = responseJson.addStatus(true).addResponseMessage("0-01-031").build(null);
				return Response.status(Response.Status.OK).entity(response).build();
			} else {
				log.info("UserID not found - " + id);
				response = responseJson.addStatus(false).addResponseMessage("0-01-020").build(null);
				return Response.status(Response.Status.NOT_FOUND).entity(response).build();
			}
		}catch(UserValidationException ex) {
			log.info("User validation failed! -  "+ ex.getMessage());
			response = responseJson.addStatus(false).addResponseMessage("0-01-025").build(null);
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

		}catch (SessionTokenInvalidException ex) {
            log.info("Session token has expired or invalid -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage("0-01-003").build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("book/create")
    public Response createBook(@Context HttpHeaders httpHeaders, Book book) {

        String response;
        try {
            if(bookService.createBook(httpHeaders, book)) {
                log.info("New book created in the system." + book);
                response = responseJson.addStatus(true).addResponseMessage("New book added- "+ book.getTitle()).build(null);
                return Response.status(Response.Status.OK).entity(response).build();
            }else {
                log.info("ISBN already exists." + book.getISBN());
                response = responseJson.addStatus(false).addResponseMessage("ISBN already exists." + book.getISBN()).build(null);
                return Response.status(Status.CONFLICT).entity(response).build();
            }
        } catch (SessionTokenInvalidException ex) {
            log.info("Session token has expired or invalid -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        } catch (InputValidationException ex) {
            log.info("invalid input -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        } catch (AuthorDoesNotExistsException ex) {
            log.info("invalid input -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("book/update")
    public Response updateBook(@Context HttpHeaders httpHeaders, Book book) {
        String response;
        try {
            if(bookService.updateBook(httpHeaders, book)){
                log.info("Book updated" + book);
                response = responseJson.addStatus(true).addResponseMessage("book updated ").build(null);
                return Response.status(Response.Status.OK).entity(response).build();
            } else {
                log.info("Book not udpated." + book);
                response = responseJson.addStatus(false).addResponseMessage("Could not update book ").build(null);
                return Response.status(Status.NOT_MODIFIED).entity(response).build();

            }
        } catch (SessionTokenInvalidException ex) {
            log.info("Session token has expired or invalid -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        } catch (InputValidationException ex) {
            log.info("invalid input -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        } catch (AuthorDoesNotExistsException ex) {
            log.info("invalid input -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("book/delete/{bookID}")
    public Response deleteBook(@Context HttpHeaders httpHeaders, @PathParam("bookID") Integer bookID) {

        String response;
        try {
            if(bookService.deleteBook(httpHeaders, bookID)) {
                log.info("Book deleted");
                response = responseJson.addStatus(true).addResponseMessage("book deleted ").build(null);
                return Response.status(Response.Status.OK).entity(response).build();
            }else {
                log.info("Book not found");
                response = responseJson.addStatus(false).addResponseMessage("book not deleted ").build(null);
                return Response.status(Status.NOT_FOUND).entity(response).build();
            }
        } catch (SessionTokenInvalidException ex) {
            log.info("Session token has expired or invalid -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("book/getBook/name/{bookName}")
    public Response getBookByName(@Context HttpHeaders httpHeaders, @PathParam("bookName") String bookName) {
        String response;
        try {
            ResponseBook book = adminService.getBookByName(httpHeaders, bookName);
            if(book != null) {
                log.info("Returning book - " + book);
                response = responseJson.addStatus(true).addResponseMessage("Returning book record for title: "+ bookName).build(book);
                return Response.status(Response.Status.OK).entity(response).build();
            }else {
                log.info("Returning book - " + book);
                response = responseJson.addStatus(false).addResponseMessage("book record not found for title:  "+ bookName).build(book);
                return Response.status(Response.Status.NOT_FOUND).entity(response).build();
            }
        } catch (SessionTokenInvalidException ex) {
            log.info("Session token has expired or invalid -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("author/add")
    public Response createAuthor(@Context HttpHeaders httpHeaders, Author author) {

        String response;
        try {
            if(adminService.createAuthor(httpHeaders,author)) {
                log.info("Author created");
                response = responseJson.addStatus(true).addResponseMessage("Author created. ").build(null);
                return Response.status(Response.Status.OK).entity(response).build();
            }else {
                log.info("Author with emailID already in system");
                response = responseJson.addStatus(true).addResponseMessage("Author with emailID already in system. ").build(null);
                return Response.status(Status.BAD_REQUEST).entity(response).build();
            }
        } catch (SessionTokenInvalidException ex) {
            log.info("Session token has expired or invalid -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        } catch (InputValidationException ex) {
            log.info("invalid input -  "+ ex.getMessage());
            response = responseJson.addStatus(false).addResponseMessage(ex.getMessage()).build(null);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }
    }





        public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}
	
	public void setResponseJson(ResponseJson responseJson) {
		this.responseJson = responseJson;
	}


}
