package com.develop.catalog.admin.ws;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.web.filter.RequestContextFilter;


public class App extends ResourceConfig {
	
	public App() {
		//application resources
		register(AdminWebServiceImpl.class);
		
		//bridge between JAX-rx and spring
		register(RequestContextFilter.class);
	}

}
