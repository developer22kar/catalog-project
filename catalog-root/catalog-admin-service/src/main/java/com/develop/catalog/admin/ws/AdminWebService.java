package com.develop.catalog.admin.ws;

import com.develop.catalog.utilities.model.Author;
import com.develop.catalog.utilities.model.Book;
import com.develop.catalog.utilities.model.User;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

/**
 * <h1>AdminWebService</h1>
 * Application interface for most of the admin operations.
 * @author Pritesh Baviskar
 * @version 1.0
 */

public interface AdminWebService {
	
	/**
	 * Login call for admin user
	 * @param user
	 * @return The method returns a session token if user credentials were correct, 
	 * else, throws appropriate error codes if user credentials are incorrect or if user is not found in the system.
	 */
	public Response loginUser(User user);

    /**
     * This call allows admins to create users/customers in the system. This is much similar to register call
     * in <codes>UserProfileService</codes>, except that the users are in a ACTIVE state as soon as they are created,
     * compared to PENDING status in the latter.
     * @param httpHeaders
     * @param user
     * @return Returns a standard response json with appropriate values for success/failures
     */
	public Response createUser(HttpHeaders httpHeaders, User user);

    /**
     * This call allows admins to update a user/customer
     * @param httpHeaders
     * @param user
     * @return
     */
	public Response updateUser(HttpHeaders httpHeaders, User user);

    /**
     * This call allows admins to delete user/customer from db. Users are set to inactive status when they are "deleted"
     * @param httpHeaders
     * @param id
     * @return Returns a standard response json with appropriate values for success/failures
     */
	public Response deleteUserById(HttpHeaders httpHeaders, Integer id);


    public Response createBook(HttpHeaders httpHeaders, Book book);

    public Response updateBook(HttpHeaders httpHeaders, Book book);

    public Response deleteBook(HttpHeaders httpHeaders, Integer bookID);

    Response createAuthor(HttpHeaders httpHeaders, Author author);

    public Response getBookByName(HttpHeaders httpHeaders, String bookName);
}
