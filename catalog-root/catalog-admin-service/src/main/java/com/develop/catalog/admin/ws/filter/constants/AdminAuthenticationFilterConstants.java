package com.develop.catalog.admin.ws.filter.constants;


public class AdminAuthenticationFilterConstants
{
	public static final String WEBCALL_GET_USER = "getUser";
	public static final String AUTHENTICATION_TOKEN = "auth_token";
	public static final String WEBCALL_LOGIN = "login";
	public static final String WEBCALL_CREATEUSER = "register";
	public static final String WEB_CALL_VERIFY = "verify";
	public static final int SESSION_EXPIRY_TIME = 20;
	public static final String MEDIA_TYPE_JSON = "application/json";
	
	public static final String INVALID_TOKEN = "Token is invalid. Go Home!";
	public static final String SESSION_EXPIRED = "Session token has expired. Please login again.";
	public static final String USER_UNAUTHORIZED = "User not authorized or user not found.";
	public static final String BAD_REQUEST = "Invalid request. Its a bad request.";
	public static final String TOKEN_NOT_FOUND = "Authentication token not found with request. No un-authorized access here. Go Home!";

}
