package com.develop.catalog.admin.service;

import com.develop.catalog.service.exception.InputValidationException;
import com.develop.catalog.service.exception.SessionTokenInvalidException;
import com.develop.catalog.service.exception.UserValidationException;
import com.develop.catalog.utilities.dao.UserAuthDAO;
import com.develop.catalog.utilities.dao.UserDAO;
import com.develop.catalog.utilities.dao.UserRoleDAO;
import com.develop.catalog.utilities.json.ResponseBook;
import com.develop.catalog.utilities.model.*;
import com.develop.catalog.ws.filter.AuthenticationFilter;
import com.develop.catalog.ws.service.AuthorService;
import com.develop.catalog.ws.service.BookService;
import com.develop.catalog.ws.service.UserProfileService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.HttpHeaders;
import java.util.List;

@Service
public class AdminServiceImpl implements AdminService{
	
	private Log log = LogFactory.getLog(AdminServiceImpl.class);
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private UserAuthDAO userAuthDAO;
	
	@Autowired
	private UserRoleDAO userRoleDAO;

	@Autowired
	private UserProfileService userProfileService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private BookService bookService;


	public String loginAdminUser(User user) {
		// check if user is an admin user first. if not, kick them out.
		if(isUserValid(user)) {
			User fetchedUser = getUserByEmailID(user);
			List<UserRole> userRoles = userRoleDAO.findByUserID(fetchedUser.getUserID());
			if(userRoles != null) {
				// admin users have userRole. is its null, they are not admin. proceed otherwise
				return userProfileService.loginUser(user);
			}
		}
		return null;
	}
	
	public User updateUser(HttpHeaders httpHeaders, User user) throws UserValidationException, SessionTokenInvalidException {
		
		String sessionToken = httpHeaders.getHeaderString(AuthenticationFilter.AUTHENTICATION_TOKEN);
		
		if(user.getFirstName() == null && user.getLastName() == null  && user.getPassword() == null)
			return null;
		
		// last modified is set to admin user
		User adminUser = getUserFromSessionToken(sessionToken);
        // only admins can do user updates through admin site
		if(adminUser != null) {
            user.setLastModifiedBy(adminUser.getUserID());
            User updatedUserName = userProfileService.updateUserName(user);
            User updatedUserPassword = userProfileService.updateUserPassword(user);
            User updatedUserPhoneNumber = userProfileService.updateUserPhoneNumber(user);

            if (updatedUserName == null && updatedUserPassword == null && updatedUserPhoneNumber == null) {
                return null;
            }
            return userDAO.findByEmailID(user.getEmailID());
            }

        throw new SessionTokenInvalidException("Session is invalid or expired");
	}

	public boolean createUser(HttpHeaders httpHeaders, User user) throws UserValidationException, SessionTokenInvalidException {

		String sessionToken = httpHeaders.getHeaderString(AuthenticationFilter.AUTHENTICATION_TOKEN);
		log.info("Session token for request: "+ sessionToken);
		// auth filter wont allow null session tokens anyway. still a paranoia check
		if(sessionToken != null && !sessionToken.isEmpty()) {
			UserAuthentication userAuthData =  userAuthDAO.findBySessionToken(sessionToken);
			if(userAuthData != null ) {
				log.info("User with valid session token is making this request. Username: " + userAuthData.getUser().getEmailID());
				user.setCreatedBy(userAuthData.getUser().getUserID());
				user.setLastModifiedBy(userAuthData.getUser().getUserID());
                // these users are created by admin with a default password of password 123. They can change it later
                user.setPassword("password123");
				if(userProfileService.createUser(user)) {
					return true;
				} 
			}
			log.info("No user authentication data found with session token. Maybe the token has expired. ");
            throw new SessionTokenInvalidException("No user authentication data found with session token. Maybe the token has expired. ");
		}
		return false;
	}
	
	public boolean deleteUserById(HttpHeaders httpHeaders, Integer userID) throws UserValidationException, SessionTokenInvalidException {
	
		String sessionToken = httpHeaders.getHeaderString(AuthenticationFilter.AUTHENTICATION_TOKEN);
		User adminUser = getUserFromSessionToken(sessionToken);
		if(adminUser != null) {
			if(userProfileService.deleteUserById(userID)) {
				// if delete was a success, update who modified this user profile.
				User user = userDAO.findById(userID);
				user.setLastModifiedBy(adminUser.getUserID());
				userDAO.update(user);
				return true;
			} else {
                return false;
            }
		}
		throw new SessionTokenInvalidException("Session token is invalid or expired.");
	}

    @Override
    public boolean createAuthor(HttpHeaders httpHeaders, Author author) throws SessionTokenInvalidException, InputValidationException {

        String sessionToken = httpHeaders.getHeaderString(AuthenticationFilter.AUTHENTICATION_TOKEN);
        User adminUser = getUserFromSessionToken(sessionToken);
        if (adminUser != null) {
            return authorService.createAuthor(adminUser, author) ? true : false;
        }
        throw new SessionTokenInvalidException("Session token is invalid or expired.");
    }

    @Override
    public ResponseBook getBookByName(HttpHeaders httpHeaders, String bookName) throws SessionTokenInvalidException {

        String sessionToken = httpHeaders.getHeaderString(AuthenticationFilter.AUTHENTICATION_TOKEN);
        User adminUser = getUserFromSessionToken(sessionToken);
        if (adminUser != null) {
            return bookService.getBookByName(bookName);
        }
        throw new SessionTokenInvalidException("Session token is invalid or expired.");
    }
    private boolean isUserEmailIDValid(String emailID) {
        return userDAO.findByEmailID(emailID) != null;
    }


	private User getUserFromSessionToken(String sessionToken) {
		// authentication filter should take care of discarding invalid session tokens. Avoiding repeated checks here. 
		UserAuthentication userAuthData =  userAuthDAO.findBySessionToken(sessionToken);
		if(userAuthData != null) {
 			if(userRoleDAO.findByUserID(userAuthData.getUser().getUserID()) != null) {
 				// this guy is a admin portal user.
 				return userAuthData.getUser();
 			}
		}
		return null;
	}

    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    private boolean isUserValid(User user) {
		return (userDAO.findByEmailID(user.getEmailID()) != null) ? true : false;
	}

	public User getUserByEmailID(User user) {
		return userDAO.findByEmailID(user.getEmailID());
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	public void setUserProfileService(UserProfileService userProfileService) {
		this.userProfileService = userProfileService;
	}

	public void setUserAuthDAO(UserAuthDAO userAuthDAO) {
		this.userAuthDAO = userAuthDAO;
	}

	public void setUserRoleDAO(UserRoleDAO userRoleDAO) {
		this.userRoleDAO = userRoleDAO;
	}


}
