package com.develop.catalog.admin.service;

import com.develop.catalog.service.exception.InputValidationException;
import com.develop.catalog.service.exception.SessionTokenInvalidException;
import com.develop.catalog.service.exception.UserValidationException;
import com.develop.catalog.utilities.json.ResponseBook;
import com.develop.catalog.utilities.model.Author;
import com.develop.catalog.utilities.model.User;

import javax.ws.rs.core.HttpHeaders;

public interface AdminService {


	boolean createUser(HttpHeaders httpHeaders, User user) throws UserValidationException, SessionTokenInvalidException, SessionTokenInvalidException;

	String loginAdminUser(User user);
	
	User updateUser(HttpHeaders httpHeaders, User user) throws UserValidationException, SessionTokenInvalidException;
	
	boolean deleteUserById(HttpHeaders httpHeaders, Integer userID) throws UserValidationException, SessionTokenInvalidException;

    boolean createAuthor(HttpHeaders httpHeaders, Author author) throws SessionTokenInvalidException, InputValidationException;

    ResponseBook getBookByName(HttpHeaders httpHeaders, String bookName) throws SessionTokenInvalidException;

}
