package com.develop.catalog.ws;

import com.develop.catalog.utilities.dao.*;
import com.develop.catalog.utilities.model.Author;
import com.develop.catalog.utilities.model.AuthorBook;
import com.develop.catalog.utilities.model.Book;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by pritesh on 4/4/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:TestApplication-context.xml"})
public class TestBookServiceImpl extends JerseyTest {

    private Book book;
    private Author author;
    private AuthorBook ab;

    @Autowired
    private UserAuthDAO userAuthenticationDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private RoleDAO roleDAO;

    @Autowired
    private UserRoleDAO userRoleDAO;

    @Autowired
    private AuthorDAO authorDAO;
    @Autowired
    private AuthorBookDAO authorBookDAO;
    @Autowired
    private BookDAO bookDAO;

    @Before
    public void setup() {
        author = new Author();
        author.setFirstName("Brian");
        author.setLastName("Goetz");
        author.setEmailID("b@gmail.com");
        authorDAO.saveOrUpdate(author);

        book = new Book();
        book.setTitle("Java concurrency");
        book.setISBN("9780201657883");
        book.setPublisher("Addison");
        book.setPrice(new BigDecimal(10));

        bookDAO.saveOrUpdate(book);

        ab = new AuthorBook();
        ab.setAuthor(author);
        ab.setBook(book);
        authorBookDAO.saveOrUpdate(ab);

    }


    @Test
    public void testGetBookSuccess() {
        assertNotNull(bookDAO.findById(1));
        Response response = target("/books/getBook/id/1").request().get();
        assertEquals(200,response.getStatus());
        String output = response.readEntity(String.class);
        System.out.println(output);
    }

    @Test
    public void testGetBookFail() {
        Response response = target("/books/getBook/id/123").request().get();
        assertEquals(404,response.getStatus());
        String output = response.readEntity(String.class);
        System.out.println(output);
    }

    @Test
    public void testGetBookByName_Success() {
        assertNotNull(bookDAO.findById(1));
        Response response = target("/books/getBook/name/Java%20ConCurReNcy").request().get();
        assertEquals(200,response.getStatus());
        String output = response.readEntity(String.class);
        System.out.println(output);
    }

    @Test
    public void testGetBookByName_Fail() {
        Response response = target("/books/getBook/name/Twilight").request().get();
        assertEquals(404,response.getStatus());
        String output = response.readEntity(String.class);
        System.out.println(output);
    }

    @Test
    public void testGetBookByAuthor() {

        Author author = new Author();
        author.setFirstName("JK");
        author.setLastName("Rowling");
        author.setEmailID("jk@gmail.com");
        authorDAO.saveOrUpdate(author);

        Book book1 = new Book();
        book1.setTitle("Harry Potter 1");
        book1.setISBN("9780201657883");
        book1.setPublisher("Addison");
        book1.setPrice(new BigDecimal(10));
        bookDAO.saveOrUpdate(book1);


        Book book2 = new Book();
        book2.setTitle("Harry Potter 2");
        book2.setISBN("9780321349606");
        book2.setPublisher("Addison");
        book2.setPrice(new BigDecimal(10));
        bookDAO.saveOrUpdate(book2);

        Book book3 = new Book();
        book3.setTitle("Harry Potter 3");
        book3.setISBN("9780596527754");
        book3.setPublisher("Addison");
        book3.setPrice(new BigDecimal(10));
        bookDAO.saveOrUpdate(book3);


        AuthorBook ab1 = new AuthorBook();
        ab1.setAuthor(author);
        ab1.setBook(book1);
        AuthorBook ab2 = new AuthorBook();
        ab2.setAuthor(author);
        ab2.setBook(book2);

        AuthorBook ab3 = new AuthorBook();
        ab3.setAuthor(author);
        ab3.setBook(book3);

        authorBookDAO.saveOrUpdate(ab1);
        authorBookDAO.saveOrUpdate(ab2);
        authorBookDAO.saveOrUpdate(ab3);


        Response response = target("/books/getBooks/author/JK%20Rowling").request().get();
        assertEquals(200,response.getStatus());
        String output = response.readEntity(String.class);
        System.out.println(output);
    }

    @Test
    public void testGetBooksByAuthor_Fail() {
        Response response = target("/books/getBooks/author/Pritesh").request().get();
        assertEquals(404,response.getStatus());
        String output = response.readEntity(String.class);
        System.out.println(output);

    }


    @Override
    protected Application configure() {
        ResourceConfig rc = new ResourceConfig(BookWebServiceImpl.class);
        rc.property("contextConfigLocation", "classpath*:TestApplication-context.xml");
        return rc;

    }

    public void setUserAuthenticationDAO(UserAuthDAO userAuthenticationDAO) {
        this.userAuthenticationDAO = userAuthenticationDAO;
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void setRoleDAO(RoleDAO roleDAO) {
        this.roleDAO = roleDAO;
    }

    public void setUserRoleDAO(UserRoleDAO userRoleDAO) {
        this.userRoleDAO = userRoleDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public void setAuthorBookDAO(AuthorBookDAO authorBookDAO) {
        this.authorBookDAO = authorBookDAO;
    }

    public void setBookDAO(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }
}
