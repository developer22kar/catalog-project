package com.develop.catalog.ws.filter;

import com.develop.catalog.utilities.dao.UserAuthDAO;
import com.develop.catalog.utilities.dao.UserDAO;
import com.develop.catalog.utilities.json.ResponseJson;
import com.develop.catalog.utilities.model.User;
import com.develop.catalog.utilities.model.UserAuthentication;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;


public class TestAuthenticationFilter {

	@Mock
	private ContainerRequestContext requestContext;
	@Mock
	private UriInfo uriInfo;
	@Mock
	private ResponseJson responseJson;
	@Mock
	private UserDAO userDAO;
	@Mock
	private UserAuthDAO userAuthenticationDAO;
	
	private ArgumentCaptor<Response> argumentCaptor;
	private AuthenticationFilter authFilter;
	
	@Before
	public void setup() {
		
		argumentCaptor = ArgumentCaptor.forClass(Response.class);
		authFilter = new AuthenticationFilter();
		MockitoAnnotations.initMocks(this);
		Mockito.when(uriInfo.getPath()).thenReturn("somepath");
		Mockito.when(requestContext.getUriInfo()).thenReturn(uriInfo);

		when(responseJson.addStatus(anyBoolean())).thenReturn(responseJson);
		when(responseJson.addResponseMessage(anyString())).thenReturn(responseJson);
		when(responseJson.build(anyObject())).thenReturn("response");
		authFilter.setResponseJson(responseJson);
		authFilter.setUserDAO(userDAO);
		authFilter.setUserAuthenticationDAO(userAuthenticationDAO);
	}
	
	
	@Test
	public void testFilter_NoToken() throws Exception {
		authFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),401);
		String json = (String) argumentCaptor.getValue().getEntity(); 
		assertTrue(json instanceof String);
	}
	
	
	@Test
	public void testFilter_NoTokenCreateMethod() throws Exception {
		Mockito.when(uriInfo.getPath()).thenReturn("../register");
		authFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
	}
	
	@Test
	public void testFilter_NoTokenVerifyMethod() throws Exception {
		Mockito.when(uriInfo.getPath()).thenReturn("../verify");
		authFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
	}
	
	@Test
	public void testFilter_NoTokenLoginMethod() throws Exception {
		Mockito.when(uriInfo.getPath()).thenReturn("../login");
		authFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
		
	}
	
	@Test
	public void testFilter_TokenCreateMethod() throws Exception {
		when(requestContext.getHeaderString("auth_token")).thenReturn("faketoken");
		Mockito.when(uriInfo.getPath()).thenReturn("../register");
		authFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
		
	}

	@Test
	public void testFilter_TokenLoginMethod() throws Exception {
		when(requestContext.getHeaderString("auth_token")).thenReturn("faketoken");
		Mockito.when(uriInfo.getPath()).thenReturn("../login");
		authFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
		
	}
	
	@Test
	public void testFilter_TokenVerifyMethod() throws Exception {
		when(requestContext.getHeaderString("auth_token")).thenReturn("faketoken");
		Mockito.when(uriInfo.getPath()).thenReturn("../verify");
		authFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
	}


	@Test
	public void testFilter_TokenInvalidAndBadRequest() throws Exception {
		InputStream is = IOUtils.toInputStream("");
		when(requestContext.getHeaderString("auth_token")).thenReturn("faketoken");
		when(requestContext.getEntityStream()).thenReturn(is);
		authFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),400);
	}

	
	@Test
	public void testFilter_TokenInvalidAndUserFromBodyNotFoundInDB() throws Exception {
		
		InputStream is = IOUtils.toInputStream("{\r\n  \"emailID\": \"pbaviskfgdfgar@shutterfly.com\",\r\n  \"password\": \"24081986Pri#\"\r\n}");
		when(requestContext.getHeaderString("auth_token")).thenReturn("faketoken");
		when(requestContext.getEntityStream()).thenReturn(is);
		when(userDAO.findByEmailID((String)anyObject())).thenReturn(null);
		
		authFilter.filter(requestContext);
		
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),403);
	}
	
	@Test
	public void testFilter_TokenInvalidAndUserFromBodyFoundInDB() throws Exception {
		
		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("pbaviskar@shutterfly.com");
		user1.setFirstName("Amar");
		user1.setPassword("aailaUiim@1");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken("correcttoken");

		
		InputStream is = IOUtils.toInputStream("{\r\n  \"emailID\": \"pbaviskar@shutterfly.com\",\r\n  \"password\": \"24081986Pri#\"\r\n}");
		when(requestContext.getHeaderString("auth_token")).thenReturn("faketoken");
		when(requestContext.getEntityStream()).thenReturn(is);
		when(userDAO.findByEmailID("pbaviskar@shutterfly.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);
		authFilter.filter(requestContext);
		
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),401);
	}

	@Test
	public void testFilter_AllGood() throws Exception {
		
		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("pbaviskar@shutterfly.com");
		user1.setFirstName("Amar");
		user1.setPassword("aailaUiim@1");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken("correcttoken");
		uAuthData.setSessionStartTime(new Date());

		
		InputStream is = IOUtils.toInputStream("{\r\n  \"emailID\": \"pbaviskar@shutterfly.com\",\r\n  \"password\": \"24081986Pri#\"\r\n}");
		when(requestContext.getHeaderString("auth_token")).thenReturn("correcttoken");
		when(requestContext.getEntityStream()).thenReturn(is);
		when(userDAO.findByEmailID("pbaviskar@shutterfly.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);
		authFilter.filter(requestContext);
		
		verify(requestContext, never()).abortWith((Response)anyObject());
	}

	@Test
	public void testFilter_TokenValidAndUserFromBodyFoundInDBButSessionInValid() throws Exception {
		
		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("pbaviskar@shutterfly.com");
		user1.setFirstName("Amar");
		user1.setPassword("aailaUiim@1");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken("correcttoken");
		uAuthData.setSessionStartTime(cal.getTime());

		
		InputStream is = IOUtils.toInputStream("{\r\n  \"emailID\": \"pbaviskar@shutterfly.com\",\r\n  \"password\": \"24081986Pri#\"\r\n}");
		when(requestContext.getHeaderString("auth_token")).thenReturn("correcttoken");
		when(requestContext.getEntityStream()).thenReturn(is);
		when(userDAO.findByEmailID("pbaviskar@shutterfly.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);
		authFilter.filter(requestContext);
		
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),401);

	}
	
	@Test
	public void testFilter_GetUserByEmailID_Success() throws IOException {

		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("pbaviskar@shutterfly.com");
		user1.setFirstName("Amar");
		user1.setPassword("aailaUiim@1");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken("correcttoken");
		uAuthData.setSessionStartTime(new Date());

		when(requestContext.getHeaderString("auth_token")).thenReturn("correcttoken");
		Mockito.when(uriInfo.getPath()).thenReturn("../getUser/pbaviskar@shutterfly.com");
		when(userDAO.findByEmailID("pbaviskar@shutterfly.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);

		authFilter.filter(requestContext);
		verify(requestContext, never()).abortWith((Response)anyObject());
	}
	
	@Test
	public void testFilter_GetUserByEmailID_Fail_NoEmailIDProvided() throws IOException {

		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("pbaviskar@shutterfly.com");
		user1.setFirstName("Amar");
		user1.setPassword("aailaUiim@1");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken("correcttoken");
		uAuthData.setSessionStartTime(new Date());

		when(requestContext.getHeaderString("auth_token")).thenReturn("correcttoken");
		Mockito.when(uriInfo.getPath()).thenReturn("../getUser/");
		when(userDAO.findByEmailID("pbaviskar@shutterfly.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);

		authFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(argumentCaptor.getValue().getStatus(),400);
	}

	@Test
	public void testFilter_GetUserByEmailID_Fail_NoEmailIDNotFound() throws IOException {

		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("pbaviskar@shutterfly.com");
		user1.setFirstName("Amar");
		user1.setPassword("aailaUiim@1");

		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken("correcttoken");
		uAuthData.setSessionStartTime(new Date());

		when(requestContext.getHeaderString("auth_token")).thenReturn("correcttoken");
		Mockito.when(uriInfo.getPath()).thenReturn("../getUser/champak@champak.com");
		when(userDAO.findByEmailID("pbaviskar@shutterfly.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);

		authFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(403,argumentCaptor.getValue().getStatus());
	}

	@Test
	public void testFilter_GetUserByEmailID_Fail_NotAValidEmailID() throws IOException {

		User user1 = new  User();
		user1.setUserID(1);
		user1.setEmailID("pbaviskar@shutterfly.com");
		user1.setFirstName("Amar");
		user1.setPassword("aailaUiim@1");
		user1.setCreatedBy(1);
		user1.setLastModifiedBy(1);
		
		UserAuthentication uAuthData = new UserAuthentication();
		uAuthData.setSessionAuthToken("correcttoken");
		uAuthData.setSessionStartTime(new Date());

		when(requestContext.getHeaderString("auth_token")).thenReturn("correcttoken");
		Mockito.when(uriInfo.getPath()).thenReturn("../getUser/champak#champaklalala");
		when(userDAO.findByEmailID("pbaviskar@shutterfly.com")).thenReturn(user1);
		when(userAuthenticationDAO.findByUserID(1)).thenReturn(uAuthData);

		authFilter.filter(requestContext);
		verify(requestContext).abortWith(argumentCaptor.capture());
		assertEquals(403,argumentCaptor.getValue().getStatus());
	}
}
