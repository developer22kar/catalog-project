package com.develop.catalog.ws.service;

import com.develop.catalog.service.exception.UserValidationException;
import com.develop.catalog.utilities.common.Crud;
import com.develop.catalog.utilities.model.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Method;

import static org.junit.Assert.*;


public class TestUserValidator {
	
	@Mock
    User user;
	
	UserValidator validator;
	
	@Before
	public void setup() {
		
		MockitoAnnotations.initMocks(this);
		Mockito.when(user.getFirstName()).thenReturn("Chironjilal");
		Mockito.when(user.getLastName()).thenReturn("Khosla");
		validator = new UserValidator();
	}
	
	@Test
	public void testValidUserName() throws UserValidationException {
		assertTrue(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test(expected=UserValidationException.class)
	public void testInvalidFirstName() throws UserValidationException {
		Mockito.when(user.getFirstName()).thenReturn("Chironji23lal");
		assertFalse(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test(expected=UserValidationException.class)
	public void testInvalidLastName() throws UserValidationException{
		Mockito.when(user.getLastName()).thenReturn("Khos###la");
		assertFalse(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test
	public void testNameWithSpaces() throws Exception {
		Mockito.when(user.getLastName()).thenReturn("     Khosla    ");
		assertTrue(validator.validateUser(user, Crud.CREATE));
		
		Method privateMethod = validator.getClass().getDeclaredMethod("getName");
		privateMethod.setAccessible(true);
		Object val = privateMethod.invoke(validator);
		assertEquals("Khosla", val.toString());
		
	}
	
	@Test
	public void testNameWithMoreSpaces() throws Exception {
		Mockito.when(user.getLastName()).thenReturn(" K h o s l a  ");
		assertTrue(validator.validateUser(user, Crud.CREATE));
		
		Method privateMethod = validator.getClass().getDeclaredMethod("getName");
		privateMethod.setAccessible(true);
		Object val = privateMethod.invoke(validator);
		assertEquals("Khosla", val.toString());
		
	}
	
	@Test
	public void testEmailID_Normal() throws UserValidationException{
		Mockito.when(user.getEmailID()).thenReturn("pri@abc.com");
		assertTrue(validator.validateUser(user, Crud.CREATE));
		
	}

	@Test
	public void testEmailID_WithNumeric() throws UserValidationException{
		Mockito.when(user.getEmailID()).thenReturn("pr23445i@abc.com");
		assertTrue(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test
	public void testEmailID_OnlyNumeric() throws UserValidationException{
		Mockito.when(user.getEmailID()).thenReturn("12356@abc.com");
		assertTrue(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test(expected=UserValidationException.class)
	public void testEmailID_NoAtTheRate() throws UserValidationException{
		Mockito.when(user.getEmailID()).thenReturn("1235abc.com");
		assertFalse(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test(expected=UserValidationException.class)
	public void testEmailID_NoDomain() throws UserValidationException{
		Mockito.when(user.getEmailID()).thenReturn("1235@om");
		assertFalse(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test
	public void testEmailID_DifferentDomain() throws UserValidationException{
		Mockito.when(user.getEmailID()).thenReturn("1235@abc34.com");
		assertTrue(validator.validateUser(user, Crud.CREATE));
	} 
	
	@Test
	public void testEmailID_IndianDomain() throws UserValidationException{
		Mockito.when(user.getEmailID()).thenReturn("pritesh@abc34.co.in");
		assertTrue(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test
	public void testEmailID_DifferentMeDomain() throws UserValidationException{
		Mockito.when(user.getEmailID()).thenReturn("1235@yedaanna.me");
		assertTrue(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test
	public void testPassword_Valid() throws UserValidationException{
		Mockito.when(user.getPassword()).thenReturn("Sejgch55z@");
		assertTrue(validator.validateUser(user, Crud.CREATE));
	}
	
	@Test(expected=UserValidationException.class)
	public void testPasswordInvalid_LessThan5Chars() throws UserValidationException{
		Mockito.when(user.getPassword()).thenReturn("Sgh");
		assertFalse(validator.validateUser(user, Crud.CREATE));
	}
	
	
	@Test(expected=UserValidationException.class)
	public void testPasswordInvalid_Whitespaces() throws UserValidationException{
		Mockito.when(user.getPassword()).thenReturn("Sgh5 dfd %$dfg4    5z@");
		assertFalse(validator.validateUser(user, Crud.CREATE));
	}
	
	
	@Test
	public void testPasswordValid_SpecialChars() throws UserValidationException{
		Mockito.when(user.getPassword()).thenReturn("A*()Az8g");
		assertTrue(validator.validateUser(user, Crud.CREATE));
	}
	

}	

