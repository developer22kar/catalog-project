package com.develop.catalog.ws.service;

import com.develop.catalog.service.exception.UserValidationException;
import com.develop.catalog.utilities.json.SearchedUsers;
import com.develop.catalog.utilities.model.User;

import java.util.List;

/**
 * <h1>UserProfileService</h1>
 * Service layer class to perform all user profile related operations. 
 * 
 * @author Pritesh Baviskar
 * @version 1.0
 *
 */
public interface UserProfileService {
	
	/**
	 * Validates and saves user information into database.
	 * @param user object to be saved. User email address and password
	 * 		  are the two mandatory fields needed to save a user object.
	 * 
	 * @return true: if user object was persisted successfully
	 * 		   false: failed to persist user object	
	 */
	public boolean register(User user) throws UserValidationException;
	
	public boolean createUser(User user) throws UserValidationException;

	public User getUser(User user);
	
	public User getUserByEmailID(String emailID);

    public SearchedUsers searchUserByEmailID(String emailID);
	
	public List<User> getAllUsers();
	
	public User updateUserName(User user) throws UserValidationException;
	
	public User updateUserPhoneNumber(User user) throws UserValidationException;
	
	public User updateUserPassword(User user) throws UserValidationException;
	
	public boolean deleteUser(User user) throws UserValidationException;
	
	public boolean deleteUserById(Integer id) throws UserValidationException;
	
	public String loginUser(User user);
	

}
