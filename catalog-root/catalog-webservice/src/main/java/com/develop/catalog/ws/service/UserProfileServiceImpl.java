package com.develop.catalog.ws.service;

import com.develop.catalog.service.exception.UserValidationException;
import com.develop.catalog.utilities.Pair;
import com.develop.catalog.utilities.common.Crud;
import com.develop.catalog.utilities.common.Role;
import com.develop.catalog.utilities.dao.UserAuthDAO;
import com.develop.catalog.utilities.dao.UserDAO;
import com.develop.catalog.utilities.json.SearchedUsers;
import com.develop.catalog.utilities.model.User;
import com.develop.catalog.utilities.model.UserAuthentication;
import com.develop.catalog.utilities.security.MessageDigest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserProfileServiceImpl implements UserProfileService {

	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private UserAuthDAO userAuthenticationDAO;

	@Autowired
	private UserValidator userValidator; 

	private Log log = LogFactory.getLog(UserProfileServiceImpl.class);

	public boolean register(User user) throws UserValidationException {

		if(userValidator.validateUser(user, Crud.CREATE)) {
			
			if(isEmailIDAvailable(user)) {
				log.info("User emailID available. Saving to db.");
				user.setCreatedBy(Role.SYSTEM.getCode());
				user.setLastModifiedBy(Role.SYSTEM.getCode());
                saveUserWithEncryptedPassword(user, Crud.CREATE, false);
				return true;
			}else {
				log.info("User already present. Nothing to do");
				return false;
			}

		}else {
			return false;
		}
	}

	public boolean createUser(User user) throws UserValidationException {

		if(userValidator.validateUser(user, Crud.CREATE)) {
			
			if(isEmailIDAvailable(user)) {
				log.info("User emailID available. Saving to db.");
				saveUserWithEncryptedPassword(user, Crud.CREATE, true);
				return true;
			}else {
				log.info("User already present. Nothing to do");
				return false;
			}
		}else {
			return false;
		}
	}

	public User getUser(Integer id) {
		return userDAO.findById(id);
	}

	public User getUser(User user) {
		return userDAO.findByEmailID(user.getEmailID());
	}
	
	public List<User> getAllUsers() {

		return userDAO.findAll(User.class);
	}

	public User getUserByEmailID(User user) {
		return userDAO.findByEmailID(user.getEmailID());
	}
	
	public User getUserByEmailID(String emailID) {
        return userDAO.findByEmailID(emailID);

	}

    public SearchedUsers searchUserByEmailID(String emailID) {
        return userDAO.searchUserByEmailID(emailID);
    }

	public User updateUserName(User user) throws UserValidationException{

		if(userValidator.validateUser(user, Crud.UPDATE)) {
			log.info("User json is valid- " + user);
			if(isUserValid(user)) {
				User fetchedUser = userDAO.findByEmailID(user.getEmailID());
				
				if(user.getFirstName() != null) {
					fetchedUser.setFirstName(user.getFirstName());
				}
				if(user.getLastName() != null) {
					fetchedUser.setLastName(user.getLastName());
				}
				userDAO.update(fetchedUser);
				return userDAO.findByEmailID(fetchedUser.getEmailID());

			} else {
				log.info("Unable to find user in system- " + user);
				return null;
			}
		}
		log.info("User validator returned a null- " + user);
		return null;
	}
	

	public User updateUserPhoneNumber(User user) throws UserValidationException{
		
		if(userValidator.validateUser(user, Crud.UPDATE)) {

			if(isUserValid(user)) {
				User fetchedUser = userDAO.findByEmailID(user.getEmailID());
				userDAO.update(fetchedUser);
				return userDAO.findByEmailID(fetchedUser.getEmailID());
				
			} else {
				log.info("Unable to find user in system- " + user);
				return null;
			}
		} 
		return null;
	}

	public User updateUserPassword(User user) throws UserValidationException{
		
		if(userValidator.validateUser(user, Crud.CREATE)) {
			if(isUserValid(user)) {
				User fetchedUser = userDAO.findByEmailID(user.getEmailID());
				
				if(user.getPassword() != null) {
					fetchedUser.setPassword(user.getPassword());
					// user is not updated by admin. pass false
					saveUserWithEncryptedPassword(fetchedUser, Crud.UPDATE, false);
				}
				return userDAO.findByEmailID(fetchedUser.getEmailID());
				
			} else {
				log.info("Unable to find user in system- " + user);
				return null;
			}
		}
		return null;
	}
	
	
	public boolean deleteUser(User user) throws UserValidationException{
		
		if(userValidator.validateUser(user, Crud.UPDATE)) {
			if(isUserValid(user)) {
				User fetchedUser = userDAO.findByEmailID(user.getEmailID());
				userDAO.update(fetchedUser);
				return true;
			} else {
				log.info("Unable to find user in system- " + user);
				return false;
			}
		}
		return false;
	}


	public boolean deleteUserById(Integer id) throws UserValidationException{
		
		User user = userDAO.findById(id);
		if(user != null && deleteUser(user)) {
			return true;
		} else {
			return false;
		}
	}
	
	public String loginUser(User user)  {

        if(isUserValid(user)) {

            User fetchedUser = getUserByEmailID(user);
            // Only active users can log in.

            String expectedPasswordHash = fetchedUser.getPassword();
            UserAuthentication userAuthData =  userAuthenticationDAO.findByUserID(fetchedUser.getUserID());
            String salt = userAuthData.getPasswordSaltKey();
            String actualPasswordHash = MessageDigest.verifyHash(user.getPassword() + salt);

            if(actualPasswordHash.equals(expectedPasswordHash)) {
                return generateAuthenticationToken(fetchedUser, userAuthData);

            }
        }
        return null;
    }
	
	
	/**
	 * Generates a unique authentication token, valid for a session. 
	 * Authentication token is based off of user <b>[user emailID + user Password (password hash from user table)] + login time (salt) </b>
	 * Please do not mess the order as it will create wrong authentication tokens which can be 
	 * difficult to debug.
	 * @param fetchedUser
	 * @param userAuthData
	 * 
	 * @return auth_token 
	 */
	private String generateAuthenticationToken(User fetchedUser, UserAuthentication userAuthData) {
		
		//we are here, means user login was a success. persist the login time in db.
		Date lastLoginTime = new Date();
		userAuthData.setLastLoginTime(lastLoginTime);
		log.info("Generating session token");
		String authenticationToken =  MessageDigest.generateHash(fetchedUser.getEmailID() + fetchedUser.getPassword() + lastLoginTime.toString());
		userAuthData.setSessionAuthToken(authenticationToken);
		
		//now that we have session auth token, persist session start time (now).
		userAuthData.setSessionStartTime(new Date());
		userAuthenticationDAO.update(userAuthData);

		return authenticationToken;
		
	}
	

	private String saveUserWithEncryptedPassword(User user, Crud crudOperation, boolean isUserCreatedByAdmin) {
		
		Pair<String, String> passwordHashWithSalt = MessageDigest.generateSecureHash(user.getPassword());
		user.setPassword(passwordHashWithSalt.getPasswordHash());
		log.info("Persisting user data");
		switch(crudOperation) {
		case CREATE:
			userDAO.saveOrUpdate(user);
			break;
		case UPDATE:
			userDAO.update(user);
			break;
		default:
			break;
		}
		
		return persistUserAuthenticationData(user, passwordHashWithSalt.getSalt(), crudOperation, isUserCreatedByAdmin);
	}


	private String persistUserAuthenticationData(User user, String salt, Crud crudOperation, boolean isUserCreatedByAdmin) {

		String verificationURL = ""; 
		
		switch(crudOperation) {
		case CREATE:
			UserAuthentication userAuth = new UserAuthentication();
			userAuth.setPasswordSaltKey(salt);
			userAuth.setUser(user);
			
			userAuth.setCreatedBy(user.getCreatedBy());
			userAuth.setLastModifiedBy(user.getLastModifiedBy());
			log.info("Persisting userauth data");
			userAuthenticationDAO.saveOrUpdate(userAuth);
			break;
		case UPDATE:
			UserAuthentication fetchedUserAuth = userAuthenticationDAO.findByUserID(user.getUserID());
			fetchedUserAuth.setPasswordSaltKey(salt);
			log.info("Updating userauth data");
			userAuthenticationDAO.update(fetchedUserAuth);
			break;
		default:
			break;
		}
		
		return verificationURL;
	}
	

		
	private boolean isUserValid(User user) {
		return (userDAO.findByEmailID(user.getEmailID()) != null) ? true : false;
	}

	private boolean isEmailIDAvailable(User user) {
		return (userDAO.findByEmailID(user.getEmailID()) != null) ? false : true;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public void setUserAuthDAO(UserAuthDAO userAuthenticationDAO) {
		this.userAuthenticationDAO = userAuthenticationDAO;
	}

	public void setUserValidator(UserValidator userValidator) {
		this.userValidator = userValidator;
	}
}
