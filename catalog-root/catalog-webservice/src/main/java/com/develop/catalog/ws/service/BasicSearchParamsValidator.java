package com.develop.catalog.ws.service;


import com.develop.catalog.utilities.dao.BaseDAO;
import com.develop.catalog.utilities.json.BasicSearchParams;

import java.util.ArrayList;
import java.util.List;

public class BasicSearchParamsValidator {

    public <T extends BaseDAO, K> boolean validateBasicSearchParams(BasicSearchParams searchParams, T dao, Class<K> clazz) {

        // / make sure that mandatory params are not null.
        // mandatory params for the moment are - searchText and statusCodeID
        boolean isSearchParamJsonValid = false;
        if(searchParams.getSearchText() != null &&
                searchParams.getPageSize() != null && searchParams.getPageNumber() != null) {
            if(searchParams.getSortByColumnName() != null) {

                if(!searchParams.getSortByColumnName().isEmpty()) {

                    List<String> columnNames = dao.getColumnNames(clazz);
                    List<String> updatedColumnList = new ArrayList<>(columnNames);
                    // can safely assume that userColumns are not null and empty. It would be a disaster otherwise.
                    String columnName = searchParams.getSortByColumnName();
                    boolean columnFound = false;
                    for(String userColumn : updatedColumnList) {
                        if(userColumn.equals(columnName)) {
                            columnFound = true;
                            break;
                        }
                    }

                    if(columnFound) {
                        isSearchParamJsonValid = true;
                    } else {
                        isSearchParamJsonValid = false;
                    }

                }else {
                    isSearchParamJsonValid = false;
                }
            } else if(searchParams.getSortByOrder() != null) {

                if (searchParams.getSortByOrder() == 1 || searchParams.getSortByOrder() == 0) {
                    isSearchParamJsonValid = true;
                } else {
                    isSearchParamJsonValid = false;
                }
            }else {
                isSearchParamJsonValid = true;
            }

        }
        return isSearchParamJsonValid;

    }
}
