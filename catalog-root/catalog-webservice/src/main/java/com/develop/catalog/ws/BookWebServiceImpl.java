package com.develop.catalog.ws;

import com.develop.catalog.utilities.json.ResponseBook;
import com.develop.catalog.utilities.json.ResponseJson;
import com.develop.catalog.ws.service.BookService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by pritesh on 4/3/17.
 */

@Component
@Path("/books")
public class BookWebServiceImpl implements BookWebService {


    @Autowired
    private BookService bookService;
    @Autowired
    private ResponseJson responseJson;

    private static Log log = LogFactory.getLog(BookWebServiceImpl.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getBook/id/{bookID}")
    public Response getBook(@PathParam("bookID") Integer bookID) {
        String response;
        ResponseBook book = bookService.getBook(bookID);
        if(book != null) {
            log.info("Returning book - " + book);
            response = responseJson.addStatus(true).addResponseMessage("Returning book record for bookID "+ bookID).build(book);
            return Response.status(Response.Status.OK).entity(response).build();
        }else {
            log.info("Returning book - " + book);
            response = responseJson.addStatus(false).addResponseMessage("book record not found for bookID "+ bookID).build(book);
            return Response.status(Response.Status.NOT_FOUND).entity(response).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getBook/name/{bookName}")
    public Response getBookByName(@PathParam("bookName") String bookName) {
        String response;
        ResponseBook book = bookService.getBookByName(bookName);
        if(book != null) {
            log.info("Returning book - " + book);
            response = responseJson.addStatus(true).addResponseMessage("Returning book record for title: "+ bookName).build(book);
            return Response.status(Response.Status.OK).entity(response).build();
        }else {
            log.info("Returning book - " + book);
            response = responseJson.addStatus(false).addResponseMessage("book record not found for title:  "+ bookName).build(book);
            return Response.status(Response.Status.NOT_FOUND).entity(response).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("getBooks/author/{authorName}")
    public Response getBooksByAuthor(@PathParam("authorName") String authorName) {
        String response;
        List<ResponseBook> books = bookService.getBooksByAuthor(authorName);
        if(books != null) {
            log.info("Returning books by - " + authorName);
            response = responseJson.addStatus(true).addResponseMessage("Returning books by: "+ authorName).buildList(books);
            return Response.status(Response.Status.OK).entity(response).build();
        }else {
            log.info("No books found by author - " + authorName);
            response = responseJson.addStatus(false).addResponseMessage("No books found by:  "+ authorName).build(books);
            return Response.status(Response.Status.NOT_FOUND).entity(response).build();
        }

    }

    public void setResponseJson(ResponseJson responseJson) {
        this.responseJson = responseJson;
    }

    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }
}
