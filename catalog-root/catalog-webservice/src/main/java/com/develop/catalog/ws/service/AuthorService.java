package com.develop.catalog.ws.service;

import com.develop.catalog.service.exception.InputValidationException;
import com.develop.catalog.service.exception.SessionTokenInvalidException;
import com.develop.catalog.utilities.model.Author;
import com.develop.catalog.utilities.model.User;

/**
 * Created by pritesh on 4/4/17.
 */
public interface AuthorService {
    public boolean createAuthor(User adminUser, Author author) throws SessionTokenInvalidException, InputValidationException;

}
