package com.develop.catalog.service.exception;

/**
 * Created by pritesh on 4/3/17.
 */
public class AuthorDoesNotExistsException extends Exception {
    public AuthorDoesNotExistsException() {

    }

    public AuthorDoesNotExistsException(String exceptionMessage) {

        super(exceptionMessage);
    }

    public AuthorDoesNotExistsException(Throwable cause) {

        super(cause);
    }

    public AuthorDoesNotExistsException(String exceptionMessage, Throwable cause) {

        super(exceptionMessage, cause);
    }


}
