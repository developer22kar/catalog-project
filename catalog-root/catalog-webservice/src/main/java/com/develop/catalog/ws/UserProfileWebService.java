package com.develop.catalog.ws;

import com.develop.catalog.utilities.model.User;

import javax.ws.rs.core.Response;



public interface UserProfileWebService {

	public Response register(User user);
	
	public Response getUserByEmailID(String user);
	
	public Response updateUserName(User user);
	
	public Response updateUserPassword(User user);

	public Response deleteUser(User user);
	
	public Response loginUser(User user);

}
