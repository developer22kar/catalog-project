package com.develop.catalog.service.exception;

/**
 * Created by pritesh on 11/5/16.
 */
public class SessionTokenInvalidException extends Exception {

    public SessionTokenInvalidException() {

    }


    public SessionTokenInvalidException(String exceptionMessage) {

        super(exceptionMessage);
    }

    public SessionTokenInvalidException(Throwable cause) {

        super(cause);
    }

    public SessionTokenInvalidException(String exceptionMessage, Throwable cause) {

        super(exceptionMessage, cause);
    }
}
