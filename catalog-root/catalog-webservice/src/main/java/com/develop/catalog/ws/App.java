package com.develop.catalog.ws;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.web.filter.RequestContextFilter;

public class App extends ResourceConfig {
	
	public App() {
		//application resources
		register(UserProfileWebServiceImpl.class);
		register(BookWebServiceImpl.class);
		//bridge between JAX-rx and spring
		register(RequestContextFilter.class);
	}

}
