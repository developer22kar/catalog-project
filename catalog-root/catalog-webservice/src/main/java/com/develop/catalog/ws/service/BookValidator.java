package com.develop.catalog.ws.service;

import com.develop.catalog.utilities.model.Book;
import org.apache.commons.validator.routines.ISBNValidator;

/**
 * Created by pritesh on 4/3/17.
 */
public class BookValidator {

    public boolean isBookValid(Book book) {
        if(book.getTitle() != null && !book.getTitle().isEmpty() && isISBNValid(book.getISBN()) &&
                book.getPrice() != null  && book.getPublisher() != null && !book.getPublisher().isEmpty()) {
            return true;
        } else
            return false;
    }

    public boolean isISBNValid(String isbn) {

        ISBNValidator isbnValidator = ISBNValidator.getInstance();
        if(isbn != null) {
            return isbnValidator.isValid(isbn);
        }
        return false;
    }
}
