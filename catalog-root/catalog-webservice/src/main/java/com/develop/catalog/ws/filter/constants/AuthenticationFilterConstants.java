package com.develop.catalog.ws.filter.constants;

public class AuthenticationFilterConstants {

	public static String INVALID_TOKEN = "Token is invalid. Go Home!";
	public static String SESSION_EXPIRED = "Session token has expired. Please login again.";
	public static String USER_UNAUTHORIZED = "User not authorized or user not found.";
	public static String BAD_REQUEST = "Invalid request. Its a bad request.";
	public static String TOKEN_NOT_FOUND = "Authentication token not found with request. No un-authorized access here. Go Home!";

}
