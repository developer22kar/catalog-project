package com.develop.catalog.ws.service;

import com.develop.catalog.service.exception.InputValidationException;
import com.develop.catalog.utilities.dao.AuthorBookDAO;
import com.develop.catalog.utilities.dao.AuthorDAO;
import com.develop.catalog.utilities.dao.BookDAO;
import com.develop.catalog.utilities.model.Author;
import com.develop.catalog.utilities.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by pritesh on 4/4/17.
 */

@Service
public class AuthorServiceImpl implements AuthorService {


    @Autowired
    private BookDAO bookDAO;

    @Autowired
    private AuthorDAO authorDAO;

    @Autowired
    private AuthorBookDAO authorBookDAO;


    @Override
    public boolean createAuthor(User adminUser, Author author) throws InputValidationException {

        if(isAuthorValid(author)) {
            if(!isAuthorAlreadyPresent(author.getEmailID())) {
                author.setCreatedBy(adminUser.getUserID());
                author.setLastModifiedBy(adminUser.getUserID());
                authorDAO.saveOrUpdate(author);
                return true;
            }
            return false;
        }
        throw new InputValidationException("Add author fName, lName and emailID");
    }

    private boolean isAuthorAlreadyPresent(String emailID) {
        Author author = authorDAO.findByEmailID(emailID);
        return author != null ? true : false;
    }


    private boolean isAuthorValid(Author author) {
        if(author.getFirstName() != null && !author.getFirstName().isEmpty() &&
                author.getLastName() != null && !author.getLastName().isEmpty() &&
                author.getEmailID() != null && !author.getEmailID().isEmpty()) {
            return true;
        }else
            return false;
    }

    public void setBookDAO(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public void setAuthorBookDAO(AuthorBookDAO authorBookDAO) {
        this.authorBookDAO = authorBookDAO;
    }
}
