package com.develop.catalog.ws.service;

import com.develop.catalog.service.exception.UserValidationException;
import com.develop.catalog.utilities.common.Crud;
import com.develop.catalog.utilities.model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.routines.EmailValidator;

public class UserValidator {


	private Log log = LogFactory.getLog(UserValidator.class);
	private String trimmedName; 

	public boolean validateUser(User user, Crud crudOperation) throws UserValidationException {

		boolean isUserValid = false;

		if(user.getFirstName() != null) {
			log.debug("Validating username- " + user.getFirstName());
			if(validateName(user.getFirstName())) {
				isUserValid = true;
			}else {
				
				throw new UserValidationException("Invalid FirstName for user.");
			}
		}

		if(user.getLastName() != null) {

			if(validateName(user.getLastName())) {
				isUserValid = true;
			}else {
				throw new UserValidationException("Invalid LastName for user.");
			}
		}

		if(user.getEmailID() != null) {

			if(validateEmailID(user.getEmailID())) {
				isUserValid = true;
				log.info("Valid emailID.");
			}else {
				throw new UserValidationException("Invalid email address for user. Please use only letters (a-z), numbers, and periods.");
			}
		}

		//db stores password hash; password validation will fail for hashes. 
		//so, validate only while creation.
		if(user.getPassword() != null && crudOperation.equals(Crud.CREATE)) {
			
			if(validatePassword(user.getPassword())) {
				isUserValid = true;
				log.info("Valid password.");
			}else {
				throw new UserValidationException("Password does not meet the criteria.");
			}
		}

		return isUserValid;
	}

	private boolean validatePhoneNumber(String phoneNumber) {

		String indianPhoneNumbersRegex = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
		return phoneNumber.matches(indianPhoneNumbersRegex);
	}

	/**
	 * 	  
	 * The method checks for following set of policies for user password-<p>
	 * 1. Alphanumeric 
	 * 2. Optional special characters
	 * 3. Minimum length 5 characters
	 * 5. No white spaces<p>
	 * @param password
	 * @return <b>true</b> if password satisfies the password policy set
	 * 		   false otherwise
	 */
	private boolean validatePassword(String password) {
		
		String passwordRegex = "^([a-zA-Z0-9?~!@#$%^&*()<>?_+-={}\\/\\]|]+)(?=\\S+$).{4,}$";
		return password.matches(passwordRegex);
	}

	private boolean validateEmailID(String emailID) {

		// Please use only letters (a-z), numbers, and periods.
		EmailValidator emailValidator = EmailValidator.getInstance();
		return emailValidator.isValid(emailID);
	}

	private boolean validateName(String name) {

		trimmedName = name.replaceAll("\\s+", "");
		String expression = "^[a-zA-Z\\s]+"; 
		return name.matches(expression);  
	}

	// test hook
	@SuppressWarnings("unused")
	private String getName() {
		return trimmedName;
	}
}
