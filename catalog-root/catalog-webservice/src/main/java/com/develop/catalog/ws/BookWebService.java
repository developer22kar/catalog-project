package com.develop.catalog.ws;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

/**
 * Created by pritesh on 4/3/17.
 */
public interface BookWebService {

    public Response getBook(@PathParam("bookID") Integer bookID);

    public Response getBookByName(@PathParam("bookName") String bookName);
}
