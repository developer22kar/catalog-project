package com.develop.catalog.service.exception;

public class InputValidationException extends Exception {

    public InputValidationException() {

    }

    public InputValidationException(String exceptionMessage) {

        super(exceptionMessage);
    }

    public InputValidationException(Throwable cause) {

        super(cause);
    }

    public InputValidationException(String exceptionMessage, Throwable cause) {

        super(exceptionMessage, cause);
    }
}
