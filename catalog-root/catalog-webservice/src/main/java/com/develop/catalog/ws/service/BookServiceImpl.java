package com.develop.catalog.ws.service;

import com.develop.catalog.service.exception.AuthorDoesNotExistsException;
import com.develop.catalog.service.exception.InputValidationException;
import com.develop.catalog.service.exception.SessionTokenInvalidException;
import com.develop.catalog.utilities.dao.*;
import com.develop.catalog.utilities.json.ResponseBook;
import com.develop.catalog.utilities.model.*;
import com.develop.catalog.ws.filter.AuthenticationFilter;
import com.google.common.collect.Lists;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.HttpHeaders;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by pritesh on 4/3/17.
 */

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookDAO bookDAO;

    @Autowired
    private AuthorDAO authorDAO;

    @Autowired
    private AuthorBookDAO authorBookDAO;

    @Autowired
    private BookValidator bookValidator;

    @Autowired
    private UserAuthDAO userAuthDAO;

    @Autowired
    private UserRoleDAO userRoleDAO;

    private static Log log = LogFactory.getLog(BookServiceImpl.class);

    @Override
    public boolean createBook(HttpHeaders httpHeaders, Book book) throws SessionTokenInvalidException, InputValidationException, AuthorDoesNotExistsException {

        String sessionToken = httpHeaders.getHeaderString(AuthenticationFilter.AUTHENTICATION_TOKEN);
        // last modified is set to admin user
        User adminUser = getUserFromSessionToken(sessionToken);
        if(adminUser != null) {
            if(bookValidator.isBookValid(book)) {
                if(!isBookAlreadyAddedInSystem(book)) {
                    // all is good so far.
                    book.setCreatedBy(adminUser.getUserID());
                    book.setLastModifiedBy(adminUser.getUserID());
                    bookDAO.saveOrUpdate(book);

                    // create only if authorMapping was successfully added.
                    if(updateAuthorBookMapping(book, adminUser, book, true)) {
                        bookDAO.delete(book);
                        throw new AuthorDoesNotExistsException("Please add author(s) first.");
                    }
                    else {
                        log.info("Book created successfully. ");
                        return true;
                    }
                } else
                    return false;
            }else {
                log.info("Book validation failed. ");
                throw new InputValidationException("Please check book params. Something is not right");
            }
        }
        throw new SessionTokenInvalidException("Session is invalid or expired");
    }

    @Override
    public ResponseBook getBook(Integer bookID) {
        if(bookID != null) {
            Book fetchedBook = bookDAO.findById(bookID);
            List<AuthorBook> authorBooks = authorBookDAO.findByBookId(bookID);
            if(fetchedBook != null && authorBooks != null) {
                List<String> authors = Lists.newArrayList();
                for(AuthorBook authorBook : authorBooks) {
                    authors.add(authorBook.getAuthor().getFirstName() +" "+ authorBook.getAuthor().getLastName());
                }
                ResponseBook responseBook = getResponseObject(fetchedBook);
                responseBook.setAuthor(authors);
                return responseBook;
            }
        }
        return null;
    }

    private ResponseBook getResponseObject(Book fetchedBook) {
        ResponseBook responseBook = new ResponseBook();
        try {
            BeanUtils.copyProperties(responseBook, fetchedBook);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return responseBook;
    }

    @Override
    public ResponseBook getBookByName(String bookName){
        if(bookName != null) {
            Book fetchedBook = bookDAO.findByName(bookName);
            if(fetchedBook != null) {
                List<AuthorBook> authorBooks = authorBookDAO.findByBookId(fetchedBook.getBookID());
                if(authorBooks != null) {
                    List<String> authors = Lists.newArrayList();
                    for(AuthorBook authorBook : authorBooks) {
                        authors.add(authorBook.getAuthor().getFirstName() + " " + authorBook.getAuthor().getLastName());
                    }
                    ResponseBook responseBook = getResponseObject(fetchedBook);
                    responseBook.setAuthor(authors);
                    return responseBook;
                }
            }
        }
        return null;
    }

    @Override
    public List<ResponseBook> getBooksByAuthor(String authorName) {

        if(authorName != null) {
            List<AuthorBook> fetchedEntries = authorBookDAO.findByAuthorName(authorName);
            if(fetchedEntries != null) {
                List<ResponseBook> responseBooks = Lists.newArrayList();

                for(AuthorBook entry : fetchedEntries) {
                    List<String> authors = Lists.newArrayList();
                    Book fetchedBook = entry.getBook();
                    ResponseBook responseBook = getResponseObject(fetchedBook);
                    List<AuthorBook> authorBooks = authorBookDAO.findByBookId(responseBook.getBookID());
                    for(AuthorBook ab : authorBooks) {
                        authors.add(ab.getAuthor().getFirstName() +" "+ ab.getAuthor().getLastName());
                    }
                    responseBook.setAuthor(authors);
                    responseBooks.add(responseBook);
                }
                return responseBooks;
            }
        }
        return null;
    }


    @Override
    public boolean updateBook(HttpHeaders httpHeaders, Book book) throws SessionTokenInvalidException, InputValidationException, AuthorDoesNotExistsException {

        String sessionToken = httpHeaders.getHeaderString(AuthenticationFilter.AUTHENTICATION_TOKEN);
        User adminUser = getUserFromSessionToken(sessionToken);
        if (adminUser != null) {
            if (book.getBookID() != null) {
                // we need book ID to update book.
                Book fetchedBook = bookDAO.findById(book.getBookID());
                if (fetchedBook != null) {
                    if (book.getAuthor() != null && !book.getAuthor().isEmpty()) {
                        if (updateAuthorBookMapping(book, adminUser, fetchedBook, false))
                            return false;
//                    } else {
//                            return false;
//                        }
                    }
                    if (book.getPublisher() != null && !book.getPublisher().isEmpty()) {
                        fetchedBook.setPublisher(book.getPublisher());
                    }
                    if (book.getTitle() != null && !book.getTitle().isEmpty()) {
                        fetchedBook.setTitle(book.getTitle());
                    }

                    if (book.getISBN() != null && !book.getISBN().isEmpty()) {
                        if (bookValidator.isISBNValid(book.getISBN())) {
                            fetchedBook.setISBN(book.getISBN());
                        } else {
                            return false;
                        }
                    }
                    if (book.getPrice() != null) {
                        fetchedBook.setPrice(book.getPrice());
                    }
                    fetchedBook.setLastModifiedBy(adminUser.getUserID());
                    bookDAO.update(fetchedBook);
                    return true;
                }
            } else {
                throw new InputValidationException("Please check book params. Something is not right");
            }
        }
        throw new SessionTokenInvalidException("Session is invalid or expired");
    }

    @Override
    public boolean deleteBook(HttpHeaders httpHeaders, Integer bookID) throws SessionTokenInvalidException {

        String sessionToken = httpHeaders.getHeaderString(AuthenticationFilter.AUTHENTICATION_TOKEN);
        User adminUser = getUserFromSessionToken(sessionToken);
        if (adminUser != null) {
            Book fetchedBook = bookDAO.findById(bookID);
            List<AuthorBook> fetchedAuthorBooks = authorBookDAO.findByBookId(bookID);
            if(fetchedBook != null && fetchedAuthorBooks != null) {
                for(AuthorBook fetchedAuthorBook : fetchedAuthorBooks) {
                    authorBookDAO.delete(fetchedAuthorBook);
                }
                bookDAO.delete(fetchedBook);
                return true;
            }else
                return false;
        }
        throw new SessionTokenInvalidException("Session is invalid or expired");
    }

    private boolean updateAuthorBookMapping(Book book, User adminUser, Book fetchedBook, boolean createFlag) {

        boolean authorPresentInDB = true;
        // authors is always a list- there can be more than 1 author for a book.
        String[] authors = book.getAuthor().split(",");
        for(String author : authors) {
            Author bookAuthor = authorDAO.findByName(author);
            if(bookAuthor != null) {
                authorPresentInDB &= true;
            }else
                authorPresentInDB &=false;
        }

        if(authorPresentInDB) {
            // clear up current mappings before adding new ones.
            if(!createFlag) {
                List<AuthorBook> currentAbs = authorBookDAO.findByBookId(fetchedBook.getBookID());
                for (AuthorBook ab : currentAbs) {
                    authorBookDAO.delete(ab);
                }
            }
            for (String author : authors) {
                Author bookAuthor = authorDAO.findByName(author);
                AuthorBook authorBook = new AuthorBook();
                authorBook.setAuthor(bookAuthor);
                authorBook.setBook(fetchedBook);
                authorBook.setCreatedBy(adminUser.getUserID());
                authorBook.setLastModifiedBy(adminUser.getUserID());
                authorBookDAO.saveOrUpdate(authorBook);
            }
        }else {
            return true;
        }
        return false;
    }


    private boolean isBookAlreadyAddedInSystem(Book book) {
        return bookDAO.findByISBN(book.getISBN()) != null ? true : false;
    }

    private User getUserFromSessionToken(String sessionToken) {
        // authentication filter should take care of discarding invalid session tokens. Avoiding repeated checks here.
        UserAuthentication userAuthData =  userAuthDAO.findBySessionToken(sessionToken);
        if(userAuthData != null) {
            if(userRoleDAO.findByUserID(userAuthData.getUser().getUserID()) != null) {
                // this guy is a admin portal user.
                return userAuthData.getUser();
            }
        }
        return null;
    }

    public void setUserAuthDAO(UserAuthDAO userAuthDAO) {
        this.userAuthDAO = userAuthDAO;
    }

    public void setUserRoleDAO(UserRoleDAO userRoleDAO) {
        this.userRoleDAO = userRoleDAO;
    }

    public void setBookDAO(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public void setAuthorBookDAO(AuthorBookDAO authorBookDAO) {
        this.authorBookDAO = authorBookDAO;
    }

    public void setBookValidator(BookValidator bookValidator) {
        this.bookValidator = bookValidator;
    }

}
