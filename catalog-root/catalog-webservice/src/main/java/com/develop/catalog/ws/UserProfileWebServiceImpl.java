package com.develop.catalog.ws;

import com.develop.catalog.service.exception.UserValidationException;
import com.develop.catalog.utilities.json.ResponseJson;
import com.develop.catalog.utilities.model.User;
import com.develop.catalog.ws.service.UserProfileService;
import com.google.common.collect.Maps;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;
import java.util.Map;


@Component
@Path("/users")
public class UserProfileWebServiceImpl implements UserProfileWebService{

	@Autowired
	private UserProfileService userProfileService;

	@Autowired
	private ResponseJson responseJson;
	
	private static Log log = LogFactory.getLog(UserProfileWebServiceImpl.class);
	private Map<String, Object> customResponseMap = Maps.newHashMap();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("register")
	public Response register(User user) {

		String response = "";
		log.info("Creating new user - " + user);
		try {
			if(userProfileService.register(user)){
				log.info("New user created - " + user);
				response = responseJson.addStatus(true).addResponseMessage("0-02-023").build(null);
				return Response.status(Status.CREATED).entity(response).build();
			} else {
				response = responseJson.addStatus(false).addResponseMessage("2-02-024").build(null);
				return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
			} 
		}catch(UserValidationException ex) {
			log.info("User validation failed- " + ex.getMessage());
			response = responseJson.addStatus(false).addResponseMessage("2-02-005").build(null);
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getUser/{emailID}")
	public Response getUserByEmailID(@PathParam("emailID") String emailID) {
		
		log.info("Get user with email - " + emailID);
		String response = "";
		User user = userProfileService.getUserByEmailID(emailID);
		if(user != null) {
			response = responseJson.addStatus(true).addResponseMessage("2-01-021").build(user);
			return Response.status(Response.Status.OK).entity(response).build();
		} else {
			response = responseJson.addStatus(false).addResponseMessage("2-01-020").build(user);
			return Response.status(Response.Status.NOT_FOUND).entity(response).build();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsers() {
		
		log.info("Returning all users ");
		String response = "";
		List<User> users = userProfileService.getAllUsers();
		if(users != null) {
			response = responseJson.addStatus(true).addResponseMessage("2-01-021").buildList(users);
			return Response.ok().entity(response).build();
		} else {
			response = responseJson.addStatus(false).addResponseMessage("2-01-020").buildList(users);
			return Response.status(Response.Status.NOT_FOUND).entity(response).build();
		}
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("updateUserName")
	public Response updateUserName(User user) {

		String response = "";
		try {
			log.info("Updating user name"+ user);
			User updatedUser = userProfileService.updateUserName(user);

			if(updatedUser != null) {
				log.info("User update successful-  "+ user);
				customResponseMap.clear();
				customResponseMap.put("firstName", updatedUser.getFirstName());
				customResponseMap.put("lastName", updatedUser.getLastName());
				response = responseJson.addStatus(true).addResponseMessage("2-02-025").buildCustomObject(User.class, customResponseMap);
				return Response.status(Response.Status.OK).entity(response).build();

			} else {
				log.info("User does not exists! -  "+ user);
				response = responseJson.addStatus(false).addResponseMessage("2-01-020").build(null);
				return Response.status(Response.Status.NOT_FOUND).entity(response).build();
			}
		}catch(UserValidationException ex) {
			log.info("User validation failed! -  "+ ex.getMessage());
			response = responseJson.addStatus(false).addResponseMessage("2-02-005").build(null);
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
		}
	}
	


	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("updatePassword")
	public Response updateUserPassword(User user) {
		
		String response = "";
		try{ 
			log.info("Updating user password"+ user);
			User updatedUser = userProfileService.updateUserPassword(user);

			if(updatedUser != null) {
				log.info("User update successful-  "+ user);
				response = responseJson.addStatus(true).addResponseMessage("2-02-025").build(null);
				return Response.status(Response.Status.OK).entity(response).build();

			} else {
				log.info("User does not exists! -  "+ user);
				response = responseJson.addStatus(false).addResponseMessage("2-02-020").build(null);
				return Response.status(Response.Status.NOT_FOUND).entity(response).build();
			}
		}catch(UserValidationException ex) {
			log.info("User validation failed! -  "+ ex.getMessage());
			response = responseJson.addStatus(false).addResponseMessage("2-02-005").build(null);
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
		}

	}
	

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("deleteUser")
	public Response deleteUser(User user) {
		
		String response = "";
		try {
			log.info("Deleting user - " + user);
			if(userProfileService.deleteUser(user)) {
				log.info("User marked as deleted in the system - " + user);
				response = responseJson.addStatus(true).addResponseMessage("2-02-026").build(null);
				return Response.status(Response.Status.OK).entity(response).build();
			} else {
				log.info("User not found - " + user);
				response = responseJson.addStatus(false).addResponseMessage("2-02-020").build(null);
				return Response.status(Response.Status.NOT_FOUND).entity(response).build();
			}
		}catch(UserValidationException ex) {
			log.info("User validation failed! -  "+ ex.getMessage());
			response = responseJson.addStatus(false).addResponseMessage("2-02-005").build(null);
			return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("login")
	public Response loginUser(User user) {
		
		String authenticationToken = userProfileService.loginUser(user);
		String response = "";
		if(authenticationToken != null) {
			log.info("User authenticated- token:: "+ authenticationToken);
			response = responseJson.addStatus(true).addResponseMessage("0-02-001").build(authenticationToken);
			return Response.status(Response.Status.OK).entity(response).build();
		} else {
			log.info("User credentials invalid. Unauthorized- Go home! ");
			response = responseJson.addStatus(false).addResponseMessage("2-02-027").build(null);
			return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
		}
	}


    public void setUserProfileService(UserProfileService userProfileService) {
		this.userProfileService = userProfileService;
	}

	public void setResponseJson(ResponseJson responseJson) {
		this.responseJson = responseJson;
	}
}
