
package com.develop.catalog.ws.filter;

import com.develop.catalog.utilities.dao.UserAuthDAO;
import com.develop.catalog.utilities.dao.UserDAO;
import com.develop.catalog.utilities.json.ResponseJson;
import com.develop.catalog.utilities.model.User;
import com.develop.catalog.utilities.model.UserAuthentication;
import com.develop.catalog.ws.filter.constants.AuthenticationFilterConstants;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Provider
@PreMatching
public class AuthenticationFilter implements ContainerRequestFilter  {


	private static final String WEBCALL_GET_USER = "getUser";
	private Log log = LogFactory.getLog(AuthenticationFilter.class);
	public static final String AUTHENTICATION_TOKEN = "auth_token";
	private static final String WEBCALL_LOGIN = "login";
    private static final String WEBCALL_BOOKS = "books";
	private static final String WEBCALL_CREATEUSER = "register";
	private static final String WEB_CALL_VERIFY = "verify";
	private static final int SESSION_EXPIRY_TIME = 20;
	private static final String MEDIA_TYPE_JSON = "application/json";

	@Autowired
    UserAuthDAO userAuthenticationDAO;

	@Autowired
    UserDAO userDAO;

	@Autowired
	ResponseJson responseJson;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {

		log.info("Verifying if the request has auth_token with it.");
		String path = requestContext.getUriInfo().getPath();
		String response = "";

		// there won't be auth tokens for with requests for new users and user-logins (for now)
		if(!path.contains(WEBCALL_BOOKS) && !path.contains(WEBCALL_LOGIN) && !path.contains(WEBCALL_CREATEUSER) && !path.contains(WEB_CALL_VERIFY) ) {

			String authToken = requestContext.getHeaderString(AUTHENTICATION_TOKEN);
			if(authToken != null) {
				log.info("Authentication token found with request. validating..");
				User userInBody = getRequestBody(requestContext);

				if(userInBody != null) {
					UserAuthentication userAuthData = getUserAuthData(userInBody);

					if(userAuthData != null) {
						String expectedToken = userAuthData.getSessionAuthToken();
						log.info("Expected session token:: " +expectedToken);
						log.info("Auth token came in:: " +authToken);

						if(expectedToken != null) {

							if(!expectedToken.equals(authToken)) {
								log.info("Token is invalid. Go Home!");
								response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.INVALID_TOKEN).build(null);
								requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(MEDIA_TYPE_JSON).build());

							} else {
								// token is valid. Check if its expired. 
								if(!isSessionValid(userAuthData)) {
									log.info("Session token has expired. Please login again.");
									response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.INVALID_TOKEN).build(null);
									requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(MEDIA_TYPE_JSON).build());
								}
							}
						}else {
							// user has never logged in. Probably a new user in system.
							log.info("Token is invalid. Go Home!");
							response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.INVALID_TOKEN).build(null);
							requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(MEDIA_TYPE_JSON).build());
						}
					} else {
						// user data or userauth data not found. return 404
						log.info("User not authorized or user not found.");
						response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.USER_UNAUTHORIZED).build(null);
						requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).entity(response).type(MEDIA_TYPE_JSON).build());
					}
				} else {
					
					//special case=  get call. check if emailID is present request URL
					if(path.contains(WEBCALL_GET_USER)) {
						String emailAddress = parsePathForEmailID(path);
						if(emailAddress != null && StringUtils.isNotEmpty(emailAddress)) {
							authenticateAndAuthorizeRequest(requestContext, emailAddress, authToken);
						} else {
							// its a bad request. authtoken without user emailID.
							log.info("Invalid request. Its a bad request.");
							response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.BAD_REQUEST).build(null);
							requestContext.abortWith(Response.status(Response.Status.BAD_REQUEST).entity(response).type(MEDIA_TYPE_JSON).build());
						}
					} else {
						// its a bad request. authtoken without user body.
						log.info("Invalid request. Its a bad request.");
						response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.BAD_REQUEST).build(null);
						requestContext.abortWith(Response.status(Response.Status.BAD_REQUEST).entity(response).type(MEDIA_TYPE_JSON).build());
					}
				}
			} else {
				//response 401.
				log.info("Authentication token not found with request. No un-authorized access here. Go Home!");
				response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.TOKEN_NOT_FOUND).build(null);
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(MEDIA_TYPE_JSON).build());
			}
		}
	}

	private void authenticateAndAuthorizeRequest(ContainerRequestContext requestContext, String emailAddress, String authToken) {
		
		String response = "";
		UserAuthentication userAuthData = getUserAuthData(emailAddress);

		if(userAuthData != null) {
			String expectedToken = userAuthData.getSessionAuthToken();
			log.info("Expected session token:: " +expectedToken);
			log.info("Auth token came in:: " +authToken);
			
			if(expectedToken != null) {

				if(!expectedToken.equals(authToken)) {
					log.info("Token is invalid. Go Home!");
					response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.INVALID_TOKEN).build(null);
					requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(MEDIA_TYPE_JSON).build());

				} else {
					// token is valid. Check if its expired. 
					if(!isSessionValid(userAuthData)) {
						log.info("Session token has expired. Please login again.");
						response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.INVALID_TOKEN).build(null);
						requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(MEDIA_TYPE_JSON).build());
					}
				}
			}else {
				// user has never logged in. Probably a new user in system.
				log.info("Token is invalid. Go Home!");
				response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.INVALID_TOKEN).build(null);
				requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(response).type(MEDIA_TYPE_JSON).build());
			}

			
		} else {
			// user data or userauth data not found. return 404
			log.info("User not authorized or user not found.");
			response = responseJson.addStatus(false).addResponseMessage(AuthenticationFilterConstants.USER_UNAUTHORIZED).build(null);
			requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).entity(response).type(MEDIA_TYPE_JSON).build());
		}
	}

	private String parsePathForEmailID(String path) {
		return path.substring(path.lastIndexOf("/")+1);
	}

	private boolean isSessionValid(UserAuthentication userAuthData) {

		Date sessionStartTime = userAuthData.getSessionStartTime();
		Date currentTime = new Date();
		long timeDifference = currentTime.getTime() - sessionStartTime.getTime();

		if(TimeUnit.MILLISECONDS.toMinutes(timeDifference) > SESSION_EXPIRY_TIME) {
			return false;
		} else {
			return true;
		}
	}

	private UserAuthentication getUserAuthData(User user) {
		String userEmailID = user.getEmailID();
		User fetchedUser = userDAO.findByEmailID(userEmailID);
		if(fetchedUser != null) {
			UserAuthentication userAuthData = userAuthenticationDAO.findByUserID(fetchedUser.getUserID());//.getSessionAuthToken();
			return userAuthData;

		}
		return null;
	}

	private UserAuthentication getUserAuthData(String userEmailID) {
		User fetchedUser = userDAO.findByEmailID(userEmailID);
		if(fetchedUser != null) {
			UserAuthentication userAuthData = userAuthenticationDAO.findByUserID(fetchedUser.getUserID());//.getSessionAuthToken();
			return userAuthData;

		}
		return null;
	}

	
	private User getRequestBody(ContainerRequestContext requestContext) {

		String json = "";
		try {
			if(requestContext.getEntityStream() != null) {
				json = IOUtils.toString(requestContext.getEntityStream());
				InputStream inputStream =  IOUtils.toInputStream(json);
				requestContext.setEntityStream(inputStream);
			} else {
				return null;
			}
			
		} catch (IOException e) {
			log.info("Error getting json:: " + e.getMessage());
		}

		User user = new Gson().fromJson(json, User.class);
		return user;
	}

	public void setUserAuthenticationDAO(UserAuthDAO userAuthenticationDAO) {
		this.userAuthenticationDAO = userAuthenticationDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public void setResponseJson(ResponseJson responseJson) {
		this.responseJson = responseJson;
	}
}
