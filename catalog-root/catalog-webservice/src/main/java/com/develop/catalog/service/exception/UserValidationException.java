package com.develop.catalog.service.exception;

public class UserValidationException extends Exception{

	private static final long serialVersionUID = 8853568602385421458L;
	
	public UserValidationException() {
		
	}
	
	public UserValidationException(String exceptionMessage) {
		
		super(exceptionMessage);
	}
	
	public UserValidationException(Throwable cause) {
		
		super(cause);
	}
	
	public UserValidationException(String exceptionMessage, Throwable cause) {
		
		super(exceptionMessage, cause);
	}
}
