package com.develop.catalog.ws.service;

import com.develop.catalog.service.exception.AuthorDoesNotExistsException;
import com.develop.catalog.service.exception.InputValidationException;
import com.develop.catalog.service.exception.SessionTokenInvalidException;
import com.develop.catalog.utilities.json.ResponseBook;
import com.develop.catalog.utilities.model.Book;

import javax.ws.rs.core.HttpHeaders;
import java.util.List;

/**
 * Created by pritesh on 4/3/17.
 */
public interface BookService {

    public boolean createBook(HttpHeaders httpHeaders, Book book) throws SessionTokenInvalidException, InputValidationException, AuthorDoesNotExistsException;

    public ResponseBook getBook(Integer bookID);

    public ResponseBook getBookByName(String bookName);

    public List<ResponseBook> getBooksByAuthor(String authorName);

    public boolean updateBook(HttpHeaders httpHeaders, Book book) throws SessionTokenInvalidException, InputValidationException, AuthorDoesNotExistsException;

    public boolean deleteBook(HttpHeaders httpHeaders, Integer bookID) throws SessionTokenInvalidException;
}
