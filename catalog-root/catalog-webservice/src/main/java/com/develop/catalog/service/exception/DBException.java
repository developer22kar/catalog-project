package com.develop.catalog.service.exception;

/**
 * Created by pritesh on 1/30/17.
 */
public class DBException extends Exception {

    public DBException() {

    }

    public DBException(String exception) {
        super(exception);
    }
}
