CREATE TABLE `usr_userrole` (
  `userRoleID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `userID` BIGINT UNSIGNED NOT NULL COMMENT 'FK usr_user table',
  `roleID` INT NOT NULL COMMENT 'FK usr_Role table',
  PRIMARY KEY (`userRoleID`),
  UNIQUE INDEX `uk_usr_user_userID_roleID` (`userID` ASC, `roleID` ASC),
  INDEX `fk_usr_role_usr_user_userID_idx` (`userID` ASC),
  INDEX `fk_usr_userrole_usr_role_roleID_idx` (`roleID` ASC),
  CONSTRAINT `fk_usr_userrole_usr_user_userID`
    FOREIGN KEY (`userID`)
    REFERENCES `usr_user` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usr_userrole_usr_role_roleID`
    FOREIGN KEY (`roleID`)
    REFERENCES `usr_role` (`roleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'Stores user to role mapping';

-- Add cBy, cOn, lBy, lOn columns
ALTER TABLE `usr_userrole` 
ADD COLUMN `createdBy` BIGINT UNSIGNED NULL AFTER `roleID`,
ADD COLUMN `createdOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `createdBy`,
ADD COLUMN `lastModifiedBy` BIGINT UNSIGNED NULL AFTER `createdOn`,
ADD COLUMN `lastModifiedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `lastModifiedBy`;

-- Apply FK to cBy, lBy columns
ALTER TABLE `usr_userrole` 
ADD INDEX `fk_usr_userrole_usr_user_cby_idx` (`createdBy` ASC),
ADD INDEX `fk_usr_userrole_usr_user_lby_idx` (`lastModifiedBy` ASC);
ALTER TABLE `usr_userrole` 
ADD CONSTRAINT `fk_usr_userrole_usr_user_cby`
  FOREIGN KEY (`createdBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_usr_userrole_usr_user_lby`
  FOREIGN KEY (`lastModifiedBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  -- Insert record in usr_userrole table
  Insert into usr_userrole (userID, roleID, createdBy, lastModifiedBy)
  values (1, 1, 1, 1);