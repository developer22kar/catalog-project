-- SQL scripts for book table.
CREATE TABLE `auth_book` (
  `bookID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `ISBN` VARCHAR(100) NOT NULL,
  `title` VARCHAR(100)  NOT NULL,
  `publisher` VARCHAR(100) NOT NULL ,
  `price` DECIMAL NOT NULL,
  `createdBy` BIGINT UNSIGNED NOT NULL,
  `createdOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastModifiedBy` BIGINT UNSIGNED NOT NULL,
  `lastModifiedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`bookID`),
  UNIQUE INDEX `uk_auth_book_title` (`title` ASC));

--  -- Add cBy, cOn, lBy, lOn columns
--  ALTER TABLE `auth_book`
--  ADD COLUMN `createdBy` BIGINT UNSIGNED NULL AFTER `price`,
--  ADD COLUMN `createdOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `createdBy`,
--  ADD COLUMN `lastModifiedBy` BIGINT UNSIGNED NULL AFTER `createdOn`,
--  ADD COLUMN `lastModifiedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `lastModifiedBy`;

-- Apply FK to cBy, lBy columns
ALTER TABLE `auth_book`
ADD INDEX `fk_auth_book_usr_user_cby_idx` (`createdBy` ASC),
ADD INDEX `fk_auth_book_usr_user_lby_idx` (`lastModifiedBy` ASC);
ALTER TABLE `auth_book`
ADD CONSTRAINT `fk_auth_book_usr_user_cby`
  FOREIGN KEY (`createdBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_auth_book_usr_user_lby`
  FOREIGN KEY (`lastModifiedBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `auth_book`
ADD CONSTRAINT `uk_auth_book` UNIQUE (ISBN);