
-- SQL scripts to create user and user auth table.

CREATE TABLE `usr_user` (
  `userID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(100) NULL,
  `lastName` VARCHAR(100)  NULL,
  `emailID` VARCHAR(100) NOT NULL COMMENT 'UK emailID',
  `password` VARCHAR(255) NOT NULL COMMENT 'encrypted password',
  `createdBy` BIGINT UNSIGNED NOT NULL,
  `createdOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastModifiedBy` BIGINT UNSIGNED NOT NULL,
  `lastModifiedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userID`),
  UNIQUE INDEX `uk_usr_user_emailID` (`emailID` ASC));

  -- --------------------------------------------------
  ALTER TABLE `usr_user`
  ADD INDEX `fk_usr_user_usr_user_cby_idx` (`createdBy` ASC),
  ADD INDEX `fk_usr_user_usr_user_lby_idx` (`lastModifiedBy` ASC);
  ALTER TABLE `usr_user`
  ADD CONSTRAINT `fk_usr_user_usr_user_cby`
    FOREIGN KEY (`createdBy`)
    REFERENCES `usr_user` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usr_user_usr_user_lby`
    FOREIGN KEY (`lastModifiedBy`)
    REFERENCES `usr_user` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;

  
 -- Insert sample data in user table 
 Insert into usr_user (firstName, lastName, emailID, `password`, createdBy, lastModifiedBy)
values ('pritesh', 'admin', 'admin@catalog.com', 'admin',1, 1);

CREATE TABLE `usr_userauth` (
  `userAuthID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `userID` BIGINT UNSIGNED NOT NULL COMMENT 'ID of the user, whose authentication details are being saved.',
  `saltKey` VARCHAR(100) NOT NULL COMMENT 'Salt key',
  `sessionAuthToken` VARCHAR(100) NULL COMMENT 'session auth token.',
  PRIMARY KEY (`userAuthID`),
  UNIQUE INDEX `userID_UNIQUE` (`userID` ASC),
  CONSTRAINT `fk_usr_userauth_usr_user_userID`
    FOREIGN KEY (`userID`)
    REFERENCES `usr_user` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'Stores user authentication info';

-- Add cBy, cOn, lBy, lOn columns
ALTER TABLE `usr_userauth`
ADD COLUMN `createdBy` BIGINT UNSIGNED NOT NULL AFTER `sessionAuthToken`,
ADD COLUMN `createdOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `createdBy`,
ADD COLUMN `lastModifiedBy` BIGINT UNSIGNED NOT NULL AFTER `createdOn`,
ADD COLUMN `lastModifiedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `lastModifiedBy`;

-- Apply FK to cBy, lBy columns
ALTER TABLE `usr_userauth`
ADD INDEX `fk_usr_userauth_usr_user_cby_idx` (`createdBy` ASC),
ADD INDEX `fk_usr_userauth_usr_user_lby_idx` (`lastModifiedBy` ASC);
ALTER TABLE `usr_userauth`
ADD CONSTRAINT `fk_usr_userauth_usr_user_cby`
  FOREIGN KEY (`createdBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_usr_userauth_usr_user_lby`
  FOREIGN KEY (`lastModifiedBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


ALTER TABLE `usr_userauth`
CHANGE COLUMN `saltKey` `passwordSaltKey` VARCHAR(100) NOT NULL COMMENT 'Salt key' ,
CHANGE COLUMN `sessionAuthToken` `sessionStartTime` TIMESTAMP NULL COMMENT 'session start time' ,
ADD COLUMN `lastLoginTime` TIMESTAMP NULL COMMENT 'last login time' AFTER `passwordSaltKey`;

ALTER TABLE `usr_userauth`
ADD COLUMN `sessionAuthToken` VARCHAR(100) NULL COMMENT 'session auth token.' AFTER `lastLoginTime`;
