-- SQL scripts for authbook table.

CREATE TABLE `auth_authorBook` (
  `authorBookID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `authorID` BIGINT UNSIGNED NOT NULL COMMENT 'FK auth_author table',
  `bookID` BIGINT UNSIGNED NOT NULL COMMENT 'FK auth_book table',
  PRIMARY KEY (`authorBookID`),
  UNIQUE INDEX `uk_auth_author_authorID_bookID` (`authorID` ASC, `bookID` ASC),
  INDEX `fk_auth_book_auth_author_authorID_idx` (`authorID` ASC),
  INDEX `fk_auth_authbook_auth_book_bookID_idx` (`bookID` ASC),
  CONSTRAINT `fk_auth_authbook_auth_author_authorID`
    FOREIGN KEY (`authorID`)
    REFERENCES `auth_author` (`authorID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_auth_authbook_auth_book_bookID`
    FOREIGN KEY (`bookID`)
    REFERENCES `auth_book` (`bookID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
COMMENT = 'Stores author to book mapping';

-- Add cBy, cOn, lBy, lOn columns
ALTER TABLE `auth_authorBook`
ADD COLUMN `createdBy` BIGINT UNSIGNED NULL AFTER `bookID`,
ADD COLUMN `createdOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `createdBy`,
ADD COLUMN `lastModifiedBy` BIGINT UNSIGNED NULL AFTER `createdOn`,
ADD COLUMN `lastModifiedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `lastModifiedBy`;

-- Apply FK to cBy, lBy columns
ALTER TABLE `auth_authorBook`
ADD INDEX `fk_auth_authorBook_usr_user_cby_idx` (`createdBy` ASC),
ADD INDEX `fk_auth_authorBook_usr_user_lby_idx` (`lastModifiedBy` ASC);
ALTER TABLE `auth_authorBook`
ADD CONSTRAINT `fk_auth_authorBook_usr_user_cby`
  FOREIGN KEY (`createdBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_auth_authorBook_usr_user_lby`
  FOREIGN KEY (`lastModifiedBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
