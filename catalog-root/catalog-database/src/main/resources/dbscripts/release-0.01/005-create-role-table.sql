CREATE TABLE `usr_role` (
  `roleID` INT NOT NULL,
  `roleName` VARCHAR(100) NOT NULL COMMENT 'name of the role',
  `roleDescription` Text COMMENT 'short description about the role',
  `createdBy` BIGINT UNSIGNED NOT NULL,
  `createdOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastModifiedBy` BIGINT UNSIGNED NOT NULL,
  `lastModifiedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`roleID`),
  UNIQUE INDEX `uk_usr_role_roleName` (`roleName` ASC));
  
ALTER TABLE `usr_role` 
ADD INDEX `fk_usr_role_usr_user_cby_idx` (`createdBy` ASC),
ADD INDEX `fk_usr_role_usr_user_lby_idx` (`lastModifiedBy` ASC);

ALTER TABLE `usr_role` 
ADD CONSTRAINT `fk_usr_role_usr_user_cby`
  FOREIGN KEY (`createdBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_usr_role_usr_user_lby`
  FOREIGN KEY (`lastModifiedBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
 -- Insert system roles in role table 
Insert into usr_role (roleID, roleName, roleDescription, createdBy, lastModifiedBy)
select 1, 'Admin user', 'Administrator role, for entire access to admin user', 1, 1 union
select 2, 'Standard user', 'Standard user role will have access to web portal', 1, 1 union
select 3, 'Guest user', 'Guest user role, for we-portal will same access as Standard user. Except that this user will NOT be able to checkout', 1, 1;