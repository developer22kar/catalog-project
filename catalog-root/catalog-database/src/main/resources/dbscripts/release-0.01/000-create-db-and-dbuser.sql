create database catalog_dev;

CREATE USER 'catalog_user'@'localhost' IDENTIFIED BY 'catalog_123@';

GRANT ALL PRIVILEGES ON catalog_dev. * TO 'catalog_user'@'localhost';

USE catalog_dev;

/* drop scripts, in case a rollback is required 
 * 
-- drop database catalog_dev;
-- drop USER 'catalog_user'@'localhost';
 */