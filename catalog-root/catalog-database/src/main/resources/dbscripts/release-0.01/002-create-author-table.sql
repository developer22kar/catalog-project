-- SQL scripts for author table.

CREATE TABLE `auth_author` (
  `authorID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(100) NOT NULL,
  `lastName` VARCHAR(100)  NOT NULL,
  `emailID` VARCHAR(100) NOT NULL COMMENT 'UK emailID',
  `createdBy` BIGINT UNSIGNED NOT NULL,
  `createdOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastModifiedBy` BIGINT UNSIGNED NOT NULL,
  `lastModifiedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`authorID`),
  UNIQUE INDEX `uk_auth_author_emailID` (`emailID` ASC));

--  -- Add cBy, cOn, lBy, lOn columns
--  ALTER TABLE `auth_author`
--  ADD COLUMN `createdBy` BIGINT UNSIGNED NULL AFTER `authorID`,
--  ADD COLUMN `createdOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `createdBy`,
--  ADD COLUMN `lastModifiedBy` BIGINT UNSIGNED NULL AFTER `createdOn`,
--  ADD COLUMN `lastModifiedOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `lastModifiedBy`;

-- Apply FK to cBy, lBy columns
ALTER TABLE `auth_author`
ADD INDEX `fk_auth_author_usr_user_cby_idx` (`createdBy` ASC),
ADD INDEX `fk_auth_author_usr_user_lby_idx` (`lastModifiedBy` ASC);
ALTER TABLE `auth_author`
ADD CONSTRAINT `fk_auth_author_usr_user_cby`
  FOREIGN KEY (`createdBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_auth_author_usr_user_lby`
  FOREIGN KEY (`lastModifiedBy`)
  REFERENCES `usr_user` (`userID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
