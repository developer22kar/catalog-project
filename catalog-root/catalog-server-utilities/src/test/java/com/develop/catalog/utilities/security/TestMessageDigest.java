package com.develop.catalog.utilities.security;

import static org.junit.Assert.*;

import com.develop.catalog.utilities.Pair;
import org.junit.Test;


public class TestMessageDigest {
	
	@Test
	public void testGenerateHash() {
		assertNotNull(MessageDigest.generateHash("pritesh"));
	}

	@Test
	public void testGenerateSecureHash() {
		assertTrue(MessageDigest.generateSecureHash("pritesh") instanceof Pair<?, ?>);
	}
	
	@Test
	public void testVerifyHash() {
		assertNotNull(MessageDigest.verifyHash("pritesh"));
	}

	
}
