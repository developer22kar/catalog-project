package com.develop.catalog.utilities.model;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by pritesh on 4/2/17.
 */

@Entity
@Table(name="auth_authorBook")
public class AuthorBook implements Serializable {
    public AuthorBook() {

    }

    private Integer authorBookID;
    private Author author;
    private Book book;
    private int createdBy;
    private Date createdOn;
    private int lastModifiedBy;
    private Date lastModifiedOn;

    @Id
    @GeneratedValue
    @Column(unique=true, nullable=false)
    public Integer getAuthorBookID() {
        return authorBookID;
    }

    public void setAuthorBookID(Integer authorBookID) {
        this.authorBookID = authorBookID;
    }


    @OneToOne
    @JoinColumn(name="authorID")
    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @OneToOne
    @JoinColumn(name = "bookID")
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Column(nullable = false)
    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    @Column(nullable=false, insertable=false, updatable=false)
    @Generated(GenerationTime.INSERT)
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Column(nullable = false)
    public int getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(int lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @UpdateTimestamp
    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    @Override
    public String toString() {
        return "AuthorBook{" +
                "authorBookID=" + authorBookID +
                ", author=" + author +
                ", book=" + book +
                ", createdBy=" + createdBy +
                ", createdOn=" + createdOn +
                ", lastModifiedBy=" + lastModifiedBy +
                ", lastModifiedOn=" + lastModifiedOn +
                '}';
    }
}
