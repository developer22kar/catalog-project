package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.json.SearchParams;
import com.develop.catalog.utilities.model.User;
import com.develop.catalog.utilities.json.SearchedUsers;
import com.google.common.collect.Lists;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Transactional
public class UserDAOImpl extends BaseDAOImpl<User, Integer> implements UserDAO {

	private static final String FIND_BY_EMAIL_HQL = "FROM User u where emailID = :emailID";
	

	@Autowired
	private SessionFactory sessionFactory;

	private Log log = LogFactory.getLog(UserDAOImpl.class);

	public void save(User user) {
		// move this later
		log.info("Saving user to DB- " + user);
		super.saveOrUpdate(user);
	}

	@SuppressWarnings("unchecked")
	public List<SearchedUsers> findBySearchParams(SearchParams params){

        Session session = sessionFactory.getCurrentSession();
        String sortByOrder;
        if(params.getSortByOrder() != null) {

            switch(params.getSortByOrder()) {
                case 1:
                    sortByOrder = "ASC";
                    break;
                case 2:
                    sortByOrder = "DESC";
                    break;
                default:
                    sortByOrder = "ASC";
                    break;
            }
        } else {
            sortByOrder = "ASC";
        }
        // choose a default column to sort on if none specified.
        String orderByClause = " order by U.";
        if(params.getSortByColumnName() == null) {
            params.setSortByColumnName("emailID");
        } else if(params.getSortByColumnName().equals("userStatus")) {
            // special case for statusCodeID
            params.setSortByColumnName("codeName");
            orderByClause = " order by C.";
        }

        Query query = session.createSQLQuery(session.getNamedQuery("genericUserSearch").getQueryString() + orderByClause + params.getSortByColumnName() + " " + sortByOrder);
        query.setFirstResult((params.getPageNumber()-1) * params.getPageSize());
        query.setMaxResults(params.getPageSize());
		query.setParameter("searchText", "%" + params.getSearchText() +"%");

		List<Integer> statusCodes = Lists.newArrayList();
		List<String> stringStatusCodes = Arrays.asList(params.getStatusCodeID().split("\\s*,\\s*"));
		// not the most smartest way to convert stringList to IntList. :/
		for(String s : stringStatusCodes) {
			statusCodes.add(Integer.valueOf(s));
		}
		query.setParameterList("statusCodeID", statusCodes);

		List<Object[]> foundUsers = query.list();
        List<SearchedUsers> searchedUsers = Lists.newArrayList();
        for(Object[] foundUser : foundUsers) {
            SearchedUsers user = new SearchedUsers();
            user.setUserID((BigInteger) foundUser[0]);
            user.setFirstName((String) foundUser[1]);
            user.setLastName((String) foundUser[2]);
            user.setEmailID((String) foundUser[3]);
            user.setPhoneNumber((String) foundUser[4]);
            user.setStatusCodeID((Integer) foundUser[5]);
            user.setUserStatus((String) foundUser[6]);
            user.setCreatedBy((String) foundUser[7]);
            user.setCreatedOn((Date) foundUser[8]);
            user.setLastModifiedBy((String) foundUser[9]);
            user.setLastModifiedOn((Date) foundUser[10]);
            searchedUsers.add(user);

        }

        if(searchedUsers != null && !searchedUsers.isEmpty()) {
			return searchedUsers;
		}
        return null;
	}


    public BigInteger countBySearchParams(SearchParams params) {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("genericUserSearch-Count");
        query.setParameter("searchText", "%" + params.getSearchText() +"%");
        List<Integer> statusCodes = Lists.newArrayList();
        List<String> stringStatusCodes = Arrays.asList(params.getStatusCodeID().split("\\s*,\\s*"));
        // not the most smartest way to convert stringList to IntList. :/
        for(String s : stringStatusCodes) {
            statusCodes.add(Integer.valueOf(s));
        }
        query.setParameterList("statusCodeID", statusCodes);

        return (BigInteger)query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
	public User findByEmailID(String emailID) {
		
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(FIND_BY_EMAIL_HQL);
		query.setParameter("emailID", emailID);
		List<User> users = query.list();

		if(users != null && !users.isEmpty()) {
			return users.get(0);
		}else {
			return null;
		}
	}



    public SearchedUsers searchUserByEmailID(String emailID) {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("getUserDetails");
        query.setParameter("emailID", emailID);
        List<Object[]> objects = query.list();

        if(objects != null && !objects.isEmpty()) {
            // since emailID is unique, no need to iterate on the list.

            for(Object[] foundUser : objects) {
                SearchedUsers user = new SearchedUsers();
                user.setUserID((BigInteger) foundUser[0]);
                user.setFirstName((String) foundUser[1]);
                user.setLastName((String) foundUser[2]);
                user.setEmailID((String) foundUser[3]);
                user.setPhoneNumber((String) foundUser[4]);
                user.setStatusCodeID((Integer) foundUser[5]);
                user.setUserStatus((String) foundUser[6]);
                user.setCreatedBy((String) foundUser[7]);
                user.setCreatedOn((Date) foundUser[8]);
                user.setLastModifiedBy((String) foundUser[9]);
                user.setLastModifiedOn((Date) foundUser[10]);

                return user;
            }
        }
        return null;
    }

    public List<String> getColumns() {
        ClassMetadata metadata = sessionFactory.getClassMetadata(User.class);
        String[] propertyNames = metadata.getPropertyNames();
        return Arrays.asList(propertyNames);
    }

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}