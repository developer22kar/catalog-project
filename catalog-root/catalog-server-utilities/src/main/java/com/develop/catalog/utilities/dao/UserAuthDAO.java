package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.model.UserAuthentication;

public interface UserAuthDAO extends BaseDAO<UserAuthentication, Integer> {
	
	public UserAuthentication findByUserID(Integer userID);
	
	public UserAuthentication findByVerificationToken(String verificationToken);
	
	public UserAuthentication findBySessionToken(String sessionToken);
	

}
