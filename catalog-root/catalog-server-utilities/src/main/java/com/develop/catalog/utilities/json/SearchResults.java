package com.develop.catalog.utilities.json;

import java.math.BigInteger;
import java.util.List;


/**
 * Created by pritesh on 11/13/16.
 */
public class SearchResults {

    private List<SearchedUsers> searchedUsers;
    private BigInteger totalCount;

    private List<?> responseList;

    public List<?> getResponseList() {
        return responseList;
    }

    public void setResponseList(List<?> responseList) {
        this.responseList = responseList;
    }

    public List<SearchedUsers> getSearchedUsers() {
        return searchedUsers;
    }

    public void setSearchedUsers(List<SearchedUsers> searchedUsers) {
        this.searchedUsers = searchedUsers;
    }

    public BigInteger getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(BigInteger totalCount) {
        this.totalCount = totalCount;
    }
}
