package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.model.Book;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by pritesh on 4/3/17.
 */
public class BookDAOImpl extends BaseDAOImpl<Book, Integer> implements BookDAO{

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public Book findByISBN(String isbn) {
        Session session = sessionFactory.getCurrentSession();
        Query query =  session.createQuery("From Book b where ISBN = :isbn");
        query.setParameter("isbn", isbn);
        List<Book> fetchedBooks = query.list();
        if(fetchedBooks != null && !fetchedBooks.isEmpty()) {
            return fetchedBooks.get(0);
        }
        return null;
    }

    @Override
    public Book findByName(String bookName) {
        Session session = sessionFactory.getCurrentSession();
        Query query =  session.createQuery("From Book b where lower(title) = :bookName");
        query.setParameter("bookName", bookName.toLowerCase());
        List<Book> fetchedBooks =  query.list();

        if(fetchedBooks != null && !fetchedBooks.isEmpty()) {
            return fetchedBooks.get(0);
        } else {
            return null;
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
