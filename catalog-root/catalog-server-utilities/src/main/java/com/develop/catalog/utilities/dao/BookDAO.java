package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.model.Book;

/**
 * Created by pritesh on 4/3/17.
 */
public interface BookDAO extends BaseDAO<Book, Integer> {

    public Book findByISBN(String isbn);

    public Book findByName(String bookName);

}
