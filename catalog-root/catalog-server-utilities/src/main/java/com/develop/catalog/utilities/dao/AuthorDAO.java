package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.model.Author;

/**
 * Created by pritesh on 4/3/17.
 */
public interface AuthorDAO extends BaseDAO<Author, Integer>{

    public Author findByName(String name);

    Author findByEmailID(String emailID);
}
