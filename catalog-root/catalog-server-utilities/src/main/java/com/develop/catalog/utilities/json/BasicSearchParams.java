package com.develop.catalog.utilities.json;

/** Base class providing bare minimum search params needed by most of the search calls.
 * Created by pritesh on 2/16/17.
 */
public class BasicSearchParams {

    private String searchText;
    private Integer pageSize;
    private Integer pageNumber;
    private String sortByColumnName;
    private Integer sortByOrder;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getSortByColumnName() {
        return sortByColumnName;
    }

    public void setSortByColumnName(String sortByColumnName) {
        this.sortByColumnName = sortByColumnName;
    }

    public Integer getSortByOrder() {
        return sortByOrder;
    }

    public void setSortByOrder(Integer sortByOrder) {
        this.sortByOrder = sortByOrder;
    }
}
