package com.develop.catalog.utilities.security;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.develop.catalog.utilities.Pair;


public class MessageDigest {

	
	private static final String ALGO_RANDOM_NUMBER_GENERATOR = "SHA1PRNG";
	private static final String ALGO_SHA_256 = "SHA-256";
	private static final String ALGO_MD5 = "MD5";
	
	private static Log log = LogFactory.getLog(MessageDigest.class);
	
	/**
	 * 
	 * generateHash - Use this method while authenticating web calls.
	 * @param Input string to generate digest for
	 * @return Message digest
	 */
	public static String generateHash(String string) {
		
		try {
				byte[] messageDigest = getDigest(string, ALGO_MD5);
				return Hex.encodeHexString(messageDigest);
		
		} catch (NoSuchAlgorithmException e) {
 
			log.info("Error generating digest- " + e.getMessage());
		}
		return null;
		
	}
	
	
	
	/**
	 * generateSecureHash - Use this method while storing user passwords digests
	 * 						in database.
	 * @param Input string to generate secure digest for.
	 * @return {@code Pair} of passwordhash with its salt
	 */
	public static Pair<String, String> generateSecureHash(String string) {
		
		try{
			SecureRandom randomizer = SecureRandom.getInstance(ALGO_RANDOM_NUMBER_GENERATOR);
			Integer pseudoRandomNumber = randomizer.nextInt();
			
			String salt = pseudoRandomNumber.toString();
			String passwordHash = Hex.encodeHexString(getDigest(string + salt.toString(), ALGO_SHA_256));
			
			return Pair.makePair(passwordHash, salt);
			
		}catch(NoSuchAlgorithmException e) {
			log.info("Error generating digest- " + e.getMessage());
		}
		
		
		return null;
	}
	
	public static String verifyHash(String string) {
		try{
			return Hex.encodeHexString(getDigest(string, ALGO_SHA_256));
			
		}catch(NoSuchAlgorithmException ex) {
			log.info("Error generating digest- " + ex.getMessage());
		}
		
		return null;
	}

    // helper method
	public static byte[] getDigest(String string, String hashAlgorithm) throws NoSuchAlgorithmException {
		
		java.security.MessageDigest messageDigestAlgo = java.security.MessageDigest.getInstance(hashAlgorithm);
		byte[] messageDigest = messageDigestAlgo.digest(string.getBytes());

		return messageDigest;
	}
}
