package com.develop.catalog.utilities.model;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="usr_userauth")
public class UserAuthentication implements Serializable {

	
	private static final long serialVersionUID = 7645398472801903198L;
	
	public UserAuthentication() {
		
	}
	
	private int userAuthID;
	private String passwordSaltKey;
	private Date lastLoginTime;
	private String sessionAuthToken;
	private Date sessionStartTime;
	private int createdBy;
	private Date createdOn;
	private int lastModifiedBy;
	private Date lastModifiedOn;
	private User user;

	@Id
	@GeneratedValue
	@Column(unique=true, nullable=false)
	public int getUserAuthID() {
		return userAuthID;
	}
	public void setUserAuthID(int userAuthID) {
		this.userAuthID = userAuthID;
	}
	
	public void setUser(User user) {
		this.user = user;
	}	
	
	@OneToOne
	@JoinColumn(name="userID")
	public User getUser() {
		return user;
	}
	
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}
	
	@Column(nullable=false)
	public String getPasswordSaltKey() {
		return passwordSaltKey;
	}
	public void setPasswordSaltKey(String passwordSaltKey) {
		this.passwordSaltKey = passwordSaltKey;
	}
	
	@Column(nullable=true)
	public Date getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	
	@Column(nullable=true)
	public Date getSessionStartTime() {
		return sessionStartTime;
	}
	public void setSessionStartTime(Date sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}
	
	@Column(nullable=true)
	public String getSessionAuthToken() {
		return sessionAuthToken;
	}
	public void setSessionAuthToken(String sessionAuthToken) {
		this.sessionAuthToken = sessionAuthToken;
	}

	@Column(nullable = false)
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(nullable=false, insertable=false, updatable=false)
	@Generated(GenerationTime.INSERT)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(nullable = false)
	public int getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(int lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	@UpdateTimestamp
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	
	@Override
	public String toString() {
		return "UserAuthentication [userAuthID=" + userAuthID
				+ ", passwordSaltKey=" + passwordSaltKey + ", lastLoginTime="
				+ lastLoginTime + ", sessionAuthToken=" + sessionAuthToken
				+ ", sessionStartTime=" + sessionStartTime + ", createdBy="
				+ createdBy + ", createdOn=" + createdOn + ", lastModifiedBy="
				+ lastModifiedBy + ", lastModifiedOn=" + lastModifiedOn
				+ ", user=" + user + "]";
	}
	
}
