package com.develop.catalog.utilities.dao;


import com.develop.catalog.utilities.model.Role;

import java.util.List;

public interface RoleDAO extends BaseDAO<Role, Integer>{
	
	public Role findByRoleName(String roleName);

    public List searchRoles(String searchText);

}
