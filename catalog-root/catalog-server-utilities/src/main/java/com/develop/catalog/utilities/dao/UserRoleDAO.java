package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.json.ResponseUserRoleList;
import com.develop.catalog.utilities.model.UserRole;

import java.util.List;

public interface UserRoleDAO extends BaseDAO<UserRole, Integer>{

	public List<UserRole> findByUserID(Integer userID);

    public List<ResponseUserRoleList> searchByUserID(Integer userID);
}
