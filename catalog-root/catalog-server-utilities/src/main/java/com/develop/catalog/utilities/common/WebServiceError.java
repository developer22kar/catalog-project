package com.develop.catalog.utilities.common;

public enum WebServiceError {
	
	DATABASE_ERROR(500, "A database error have occured while making this call"),
	NOT_FOUND(404, "Resouce not found on server"),
	INTERNAL_SERVER_ERROR(500, "Internal server error has occured processing this request.");
	
	private int code;
	private String message;
	
	private WebServiceError(int code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public int getErrorCode() {
		return code;
	}
	
	public String getErrorMessage() {
		return message;
	}
	

}
