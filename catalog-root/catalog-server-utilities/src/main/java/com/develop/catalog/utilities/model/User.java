package com.develop.catalog.utilities.model;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name="usr_user")
public class User implements Serializable{

	private static final long serialVersionUID = -6859256197829912153L;

	public User() {

	}

	public User(String emailID, String password) {
		this.emailID = emailID;
		this.password = password;
	}
	
	private int userID;
	private String firstName;
	private String lastName;
	private String emailID;
	private String password;
	private int createdBy;
	private Date createdOn;
	private int lastModifiedBy;
	private Date lastModifiedOn;


	@Column(nullable = true)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(nullable = true)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(unique= true, nullable = false)
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	@Column(nullable = false)
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Column(nullable = false)
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	@Column(nullable = false)
	public int getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(int lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}


	@Id
	@GeneratedValue
	@Column(unique=true, nullable=false)
	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}


	@Column(nullable=false, insertable=false, updatable=false)
	@Generated(GenerationTime.INSERT)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@UpdateTimestamp
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

    @Override
    public String toString() {
        return "User{" +
                "userID=" + userID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", emailID='" + emailID + '\'' +
                ", password='" + password + '\'' +
                ", createdBy=" + createdBy +
                ", createdOn=" + createdOn +
                ", lastModifiedBy=" + lastModifiedBy +
                ", lastModifiedOn=" + lastModifiedOn +
                '}';
    }
}
