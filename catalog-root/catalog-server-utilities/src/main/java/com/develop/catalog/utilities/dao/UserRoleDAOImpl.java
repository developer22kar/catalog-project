package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.json.ResponseUserRoleList;
import com.develop.catalog.utilities.model.UserRole;
import com.google.common.collect.Lists;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class UserRoleDAOImpl extends BaseDAOImpl<UserRole, Integer> implements UserRoleDAO {

	private static String FIND_BY_USERID_HQL = "FROM UserRole where userID = :userID";
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<UserRole> findByUserID(Integer userID) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(FIND_BY_USERID_HQL);
		query.setParameter("userID", userID);
		List<UserRole> userRoles = query.list();
		
		if(userRoles != null && !userRoles.isEmpty()){
			return userRoles;
		}
		return null;
	}

    public List<ResponseUserRoleList> searchByUserID(Integer userID) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("getUserRoleList");
        query.setParameter("userID", userID);

        List<Object[]> objects = query.list();
        List<ResponseUserRoleList> responseList = Lists.newArrayList();
        if(objects != null && !objects.isEmpty()) {
            for(Object[] object : objects) {
                ResponseUserRoleList responseUserRole = new ResponseUserRoleList();
                responseUserRole.setUserRoleID((BigInteger) object[0]);
                responseUserRole.setUserID((BigInteger) object[1]);
                responseUserRole.setRoleID((Integer) object[2]);
                responseUserRole.setRoleName((String) object[3]);
                responseUserRole.setRoleDescription((String) object[4]);
                responseUserRole.setCreatedBy((String) object[5]);
                responseUserRole.setCreatedOn((Date) object[6]);
                responseUserRole.setLastModifiedBy((String) object[7]);
                responseUserRole.setLastModifiedOn((Date)object[8]);

                responseList.add(responseUserRole);

            }
        }

        if(!responseList.isEmpty()) {
            return  responseList;
        }
        return null;


    }
}
