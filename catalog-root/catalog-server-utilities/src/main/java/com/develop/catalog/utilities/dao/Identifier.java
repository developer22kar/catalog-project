package com.develop.catalog.utilities.dao;

import java.io.Serializable;

public interface Identifier<I extends Serializable> {
	
	public void setId(I identifier);
	
	public I getId();

}
