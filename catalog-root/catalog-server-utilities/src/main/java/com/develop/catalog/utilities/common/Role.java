package com.develop.catalog.utilities.common;

public enum Role {
	
	ADMIN(0),
	SYSTEM(1);
	
	private int code; 
	
	Role(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
