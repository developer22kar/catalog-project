package com.develop.catalog.utilities.model;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="usr_userrole")
public class UserRole implements Serializable{

	private static final long serialVersionUID = 8393728497348806264L;
	
	public UserRole() {
		
	}
	
	private int userRoleID;
	private User user;
	private Role role;
	private int createdBy;
	private Date createdOn;
	private int lastModifiedBy;
	private Date lastModifiedOn;

	@Id
	@GeneratedValue
	@Column(unique=true, nullable=false)
	public int getUserRoleID() {
		return userRoleID;
	}
	public void setUserRoleID(int userRoleID) {
		this.userRoleID = userRoleID;
	}

    @OneToOne
    @JoinColumn(name="userID")
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

    @OneToOne
	@JoinColumn(name="roleID")
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	
	@Column(nullable = false)
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(nullable=false, insertable=false, updatable=false)
	@Generated(GenerationTime.INSERT)
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(nullable = false)
	public int getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(int lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	@UpdateTimestamp
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	public void setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	@Override
	public String toString() {
		return "UserRole [userRoleID=" + userRoleID + ", user=" + user
				+ ", role=" + role + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", lastModifiedBy="
				+ lastModifiedBy + ", lastModifiedOn=" + lastModifiedOn + "]";
	}

}
