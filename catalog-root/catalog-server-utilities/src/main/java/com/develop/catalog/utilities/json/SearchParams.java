package com.develop.catalog.utilities.json;

import java.io.Serializable;

public class SearchParams implements Serializable {

	private static final long serialVersionUID = 3284428368095229486L;
	
	private String searchText;
	private String statusCodeID;
	private Integer pageSize;
	private Integer pageNumber;
	private String sortByColumnName;
	private Integer sortByOrder;
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getStatusCodeID() {
		return statusCodeID;
	}
	public void setStatusCodeID(String statusCodeID) {
		this.statusCodeID = statusCodeID;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getSortByColumnName() {
		return sortByColumnName;
	}
	public void setSortByColumnName(String sortByColumnName) {
		this.sortByColumnName = sortByColumnName;
	}
	public Integer getSortByOrder() {
		return sortByOrder;
	}
	public void setSortByOrder(Integer sortByOrder) {
		this.sortByOrder = sortByOrder;
	}
	
	@Override
	public String toString() {
		return "SearchParams [searchText=" + searchText + ", statusCodeID="
				+ statusCodeID + ", pageSize=" + pageSize + ", pageNumber="
				+ pageNumber + ", sortByColumnName=" + sortByColumnName
				+ ", sortByOrder=" + sortByOrder + "]";
	}
	
	
}
