package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.model.UserAuthentication;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class UserAuthDAOImpl extends BaseDAOImpl<UserAuthentication, Integer> implements UserAuthDAO{

	private static String FIND_BY_USERID_HQL = "FROM UserAuthentication where userID = :userID";
	private static String FIND_BY_VERIFICATION_TOKEN_HQL = "FROM UserAuthentication where verificationToken = :verificationToken";
	private static String FIND_BY_SESSION_TOKEN_HQL = "FROM UserAuthentication where sessionAuthToken = :sessionAuthToken";
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public UserAuthentication findByUserID(Integer userID){

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(FIND_BY_USERID_HQL);
		query.setParameter("userID", userID);
		List<UserAuthentication> userAuthentications = query.list();

		if(userAuthentications != null && !userAuthentications.isEmpty()){
			return userAuthentications.get(0);
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public UserAuthentication findByVerificationToken(String verificationToken) {

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(FIND_BY_VERIFICATION_TOKEN_HQL);
		query.setParameter("verificationToken", verificationToken);
		List<UserAuthentication> userAuthentications = query.list();

		if(userAuthentications != null && !userAuthentications.isEmpty()){
			return userAuthentications.get(0);
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public UserAuthentication findBySessionToken(String sessionToken) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(FIND_BY_SESSION_TOKEN_HQL);
		query.setParameter("sessionAuthToken", sessionToken);
		List<UserAuthentication> userAuthentications = query.list();

		if(userAuthentications != null && !userAuthentications.isEmpty()){
			return userAuthentications.get(0);
		}
		return null;
	}


    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}