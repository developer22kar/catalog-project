package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.json.ResponseRoleList;
import com.develop.catalog.utilities.model.Role;
import com.google.common.collect.Lists;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

public class RoleDAOImpl extends BaseDAOImpl<Role, Integer> implements RoleDAO {

	private static final String FIND_BY_ROLENAME_HQL = "FROM Role r where roleName = :roleName";
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public Role findByRoleName(String roleName) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(FIND_BY_ROLENAME_HQL);
		query.setParameter("roleName", roleName);
		List<Role> roles = query.list();
		
		if(roles != null && !roles.isEmpty()) {
			return roles.get(0);
		}else {
			return null;
		}
	}

    public List searchRoles(String searchText) {

        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery("getAllRoles");
        query.setParameter("searchText", "%"+ searchText + "%");
        List<Object[]> objects = query.list();
        List<ResponseRoleList> roles = Lists.newArrayList();

        if(objects != null && !objects.isEmpty()) {
            for(Object[] object : objects) {
                ResponseRoleList role = new ResponseRoleList();
                role.setRoleID((Integer) object[0]);
                role.setRoleName((String) object[1]);
                role.setRoleDescription((String) object[2]);
                role.setCreatedBy((String) object[3]);
                role.setCreatedOn((Date) object[4]);
                role.setLastModifiedBy((String) object[5]);
                role.setLastModifiedOn((Date) object[6]);

                roles.add(role);
            }
            return roles;
        }

        return null;
    }

	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
