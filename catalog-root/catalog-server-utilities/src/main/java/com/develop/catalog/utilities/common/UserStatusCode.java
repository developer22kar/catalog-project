package com.develop.catalog.utilities.common;

public enum UserStatusCode {

	ACTIVE(10001),
	
	PENDING(10002),
	
	DELETED(10003);

	private int statusCode;

	UserStatusCode(int code) {

		this.statusCode = code;
	}

	public int getStatusCode() {
		return statusCode;
	}

}
