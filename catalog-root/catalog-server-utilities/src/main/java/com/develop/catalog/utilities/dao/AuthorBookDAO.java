package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.model.AuthorBook;

import java.util.List;

/**
 * Created by pritesh on 4/3/17.
 */
public interface AuthorBookDAO extends BaseDAO<AuthorBook, Integer>  {

    public List<AuthorBook> findByBookId(Integer bookID);

    public List<AuthorBook> findByAuthorName(String authorName);
}
