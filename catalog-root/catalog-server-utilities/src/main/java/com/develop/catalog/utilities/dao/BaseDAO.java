package com.develop.catalog.utilities.dao;

import java.io.Serializable;
import java.util.List;


/*
 * Base interface for all CRUD operations
 */

public interface BaseDAO<T , I extends Serializable > {
	
	public T findById(I id);
    
	public void delete(T obj);
    
	public void saveOrUpdate(T obj);
	
	public void update(T obj);
	
	public List<T> findAll(Class<T> clazz);
	
	public Integer getAllCount(Class<T> clazz);

    public List<String> getColumnNames(Class<T> clazz);
  
}
