package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.model.Author;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by pritesh on 4/3/17.
 */
public class AuthorDAOImpl extends BaseDAOImpl<Author, Integer> implements AuthorDAO{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Author findByName(String name) {
        Session session = sessionFactory.getCurrentSession();

        String fName="", lName="";
        String[] authorName = name.split(" ");
        if(authorName.length >= 2) {
            fName = authorName[0];
            lName = authorName[1];
        }
        Criteria query  = session.createCriteria(Author.class);
        query.add(Restrictions.conjunction()
                .add(Restrictions.like("firstName",fName))
                .add(Restrictions.like("lastName",lName)));

        List<Author> authors = query.list();
        if(authors != null && !authors.isEmpty()) {
            return authors.get(0);
        }
        return null;
    }

    @Override
    public Author findByEmailID(String emailID) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("From Author where emailID = :emailID");
        query.setParameter("emailID",emailID);

        List<Author> authors = query.list();
        if(authors != null && !authors.isEmpty()) {
            return authors.get(0);
        }
        return null;
    }

    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
