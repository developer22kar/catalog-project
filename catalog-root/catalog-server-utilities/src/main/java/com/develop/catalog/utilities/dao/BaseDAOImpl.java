package com.develop.catalog.utilities.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

@Transactional
public abstract class BaseDAOImpl<T , I extends Serializable> implements BaseDAO<T, I>{

	@Autowired
	private SessionFactory sessionFactory;

	private Class<T> entityClass;
	
	@SuppressWarnings("unchecked")
	public BaseDAOImpl() {
		this.entityClass = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
    public T findById(I id) {
        return (T) sessionFactory.getCurrentSession().get(entityClass, id);
    }
	
	@Transactional
	public void delete(T object) {
		sessionFactory.getCurrentSession().delete(object);
	}
	
	@Transactional
	public void saveOrUpdate(T object) {
		sessionFactory.getCurrentSession().save(object);
	}
	
	@Transactional
	public void update(T object) {
		sessionFactory.getCurrentSession().update(object);
	}
	
	@SuppressWarnings({ "unchecked" })
	@Transactional(readOnly = true) 
	public List<T> findAll(Class<T> clazz) {
		return sessionFactory.getCurrentSession().createCriteria(clazz).list();
	}
	
	@Transactional
	public Integer getAllCount(Class<T> clazz) {
		return ((Number) sessionFactory.getCurrentSession().createCriteria(clazz).setProjection(Projections.rowCount()).uniqueResult()).intValue();
	}

    public List<String> getColumnNames(Class<T> clazz){
        ClassMetadata metadata = sessionFactory.getClassMetadata(clazz);
        String[] propertyNames = metadata.getPropertyNames();
        return Arrays.asList(propertyNames);
    }

    public Class<T> getEntityClass() {
		
		return entityClass; 
	}
	
	protected SessionFactory getSessionFactory() {

		if(sessionFactory == null) {
			throw new IllegalStateException("Session factory not set. ");
		}
		return sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
}
