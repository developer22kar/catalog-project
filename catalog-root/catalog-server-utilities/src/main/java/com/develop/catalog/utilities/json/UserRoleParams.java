package com.develop.catalog.utilities.json;

import java.io.Serializable;

/**
 * Created by pritesh on 10/31/16.
 */
public class UserRoleParams implements Serializable {

    private static final long serialVersionUID = 32844283680952296L;

    private Integer userID;
    private Integer roleID;
    private Integer userRoleID;

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Integer getRoleID() {
        return roleID;
    }

    public void setRoleID(Integer roleID) {
        this.roleID = roleID;
    }

    public Integer getUserRoleID() {
        return userRoleID;
    }

    public void setUserRoleID(Integer userRoleID) {
        this.userRoleID = userRoleID;
    }

    @Override
    public String toString() {
        return "UserRoleParams{" +
                "userID=" + userID +
                ", roleID=" + roleID +
                ", userRoleID=" + userRoleID +
                '}';
    }
}
