package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.json.SearchParams;
import com.develop.catalog.utilities.model.User;
import com.develop.catalog.utilities.json.SearchedUsers;

import java.math.BigInteger;
import java.util.List;

public interface UserDAO extends BaseDAO<User, Integer> {

	//specific queries related to User entity.

    public SearchedUsers searchUserByEmailID(String emailID);
	
	public User findByEmailID(String emailID);
	
	public List<SearchedUsers> findBySearchParams(SearchParams params);

    public BigInteger countBySearchParams(SearchParams params);

    public List<String> getColumns();


}
