package com.develop.catalog.utilities.dao;

import com.develop.catalog.utilities.model.AuthorBook;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by pritesh on 4/3/17.
 */
public class AuthorBookDAOImpl extends BaseDAOImpl<AuthorBook, Integer> implements AuthorBookDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<AuthorBook> findByBookId(Integer bookID) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("From AuthorBook b where book.bookID= :bookID");
        query.setParameter("bookID",bookID);
        List<AuthorBook> authorBookList = query.list();
        if(authorBookList != null && !authorBookList.isEmpty()) {
            return authorBookList;
        }
        return null;
    }

    @Override
    public List<AuthorBook> findByAuthorName(String authorName) {
        String fName="", lName="";
        String name[] = authorName.split(" ");
        if(name.length >= 2) {
            fName = name[0];
            lName = name[1];
        }
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("From AuthorBook b where lower(author.firstName) = :fName and lower(author.lastName) = :lName");
        query.setParameter("fName",fName.toLowerCase());
        query.setParameter("lName",lName.toLowerCase());

        List<AuthorBook> authorBookList = query.list();
        if(authorBookList != null && !authorBookList.isEmpty()) {
            return authorBookList;
        }
        return null;
    }


    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
